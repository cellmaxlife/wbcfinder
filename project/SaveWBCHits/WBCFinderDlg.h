
// WBCFinderDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "RGNData.h"
#include "Log.h"
#include <vector>
#include "CTCParams.h"
#include "SingleChannelTIFFData.h"

using namespace std;


// CWBCFinderDlg dialog
class CWBCFinderDlg : public CDialogEx
{
// Construction
public:
	CWBCFinderDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_WBCFINDER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	BYTE GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff);
	void PaintImages();
	int LoadRegionData(CString filename, vector<CRGNData *> *wbcList);

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_Display;
	CString m_Status;
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedLoad();
	CString m_SampleName;
	int m_TotalHits;
	vector<CRGNData *> m_RGNDataList;
	void CopyToRGBImage();
	CCTCParams m_CTCParams;
	void UpdateImageDisplay();
	CLog m_Log;
	CImage m_Image;
	CSingleChannelTIFFData *m_RedTIFFData;
	CSingleChannelTIFFData *m_GreenTIFFData;
	CSingleChannelTIFFData *m_BlueTIFFData;
	afx_msg void OnBnClickedSavergn();
	afx_msg LRESULT OnMyMessage(WPARAM wparam, LPARAM lparam);
	void FreeRGNList(vector<CRGNData *> *rgnList);
	afx_msg void OnBnClickedBatch();
	void EnableButtons(BOOL enable);
	bool LoadImagesAndSaveHitFile(CString sampleName, CString imagepathname, CString imagefilename, CString regionfilename);
};
