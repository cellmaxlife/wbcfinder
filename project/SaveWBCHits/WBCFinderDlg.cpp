﻿
// WBCFinderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "WBCFinder.h"
#include "WBCFinderDlg.h"
#include "afxdialogex.h"
#include "VersionNumber.h"
#include "SingleRegionImageData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#define NUM_IMAGE_COLUMNS	4
#define NUM_IMAGE_ROWS		4
#define NUM_IMAGES			16
#define SAMPLEFRAMEWIDTH  100
#define SAMPLEFRAMEHEIGHT 100
#define WM_MY_MESSAGE (WM_USER+1001)

// CWBCFinderDlg dialog

static DWORD WINAPI SaveHitBatchRun(LPVOID param);

CWBCFinderDlg::CWBCFinderDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CWBCFinderDlg::IDD, pParent)
	, m_Status(_T(""))
	, m_SampleName(_T(""))
	, m_TotalHits(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_Image.Create(NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH, -(NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT), 24);
}

void CWBCFinderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DISPLAY, m_Display);
	DDX_Text(pDX, IDC_STATUS, m_Status);
	DDX_Text(pDX, IDC_IMAGEFILE, m_SampleName);
	DDX_Text(pDX, IDC_TOTALHITS, m_TotalHits);
}

BEGIN_MESSAGE_MAP(CWBCFinderDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CWBCFinderDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_LOAD, &CWBCFinderDlg::OnBnClickedLoad)
	ON_BN_CLICKED(IDC_SAVERGN, &CWBCFinderDlg::OnBnClickedSavergn)
	ON_BN_CLICKED(IDC_BATCH, &CWBCFinderDlg::OnBnClickedBatch)
	ON_MESSAGE(WM_MY_MESSAGE, OnMyMessage)
END_MESSAGE_MAP()


// CWBCFinderDlg message handlers

BOOL CWBCFinderDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CString version;
	version.Format(_T("SaveCTCHits(%s)"), CELLMAPPLUS_VERSION);
	m_Log.NewLog(version);
	SetWindowText(version);
	version.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
	m_CTCParams.LoadCTCParams(version);
	m_RedTIFFData = new CSingleChannelTIFFData();
	m_GreenTIFFData = new CSingleChannelTIFFData();
	m_BlueTIFFData = new CSingleChannelTIFFData();
	m_Status = _T("Please Load Image");
	UpdateData(FALSE);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CWBCFinderDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		PaintImages();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CWBCFinderDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CWBCFinderDlg::PaintImages()
{
	if ((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && ((m_Image.GetHeight() == NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT)))
	{
		CPaintDC dc(&m_Display);
		CRect rect;
		m_Display.GetClientRect(&rect);
		dc.SetStretchBltMode(HALFTONE);
		m_Image.StretchBlt(dc.m_hDC, rect);
		CDC* pDC = m_Display.GetDC();
		CPen CursorPen(PS_SOLID, 3, RGB(255, 255, 255));
		CPen *pOldPen = pDC->SelectObject(&CursorPen);
		int blockWidth = (rect.right - rect.left + 1) / NUM_IMAGE_COLUMNS;
		for (int i = 1; i < NUM_IMAGE_COLUMNS; i++)
		{
			pDC->MoveTo(rect.left + i * blockWidth, rect.top);
			pDC->LineTo(rect.left + i * blockWidth, rect.bottom);
		}
		int blockHeight = (rect.bottom - rect.top + 1) / NUM_IMAGE_ROWS;
		for (int i = 1; i < NUM_IMAGE_ROWS; i++)
		{
			pDC->MoveTo(rect.left, rect.top + i * blockHeight);
			pDC->LineTo(rect.right, rect.top + i * blockHeight);
		}
		pDC->SelectObject(pOldPen);
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextAlign(TA_LEFT);
		for (int i = 0, startIndex=0; i < NUM_IMAGES; i++, startIndex++)
		{
			for (startIndex; startIndex < m_TotalHits; startIndex++)
				if (m_RGNDataList[startIndex]->GetColorCode() == CTC2)
					break;
			if (startIndex < m_TotalHits)
			{
				int ii = i / NUM_IMAGE_COLUMNS;
				int jj = i % NUM_IMAGE_COLUMNS;
				CRect rect1;
				rect1.left = rect.left + jj * blockWidth + 10;
				rect1.top = rect.top + ii * blockHeight + 10;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				CString label;
				label.Format(_T("%d"), (startIndex + 1));
				pDC->SetTextColor(RGB(255, 255, 255));
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
			}
		}
	}
}

void CWBCFinderDlg::UpdateImageDisplay()
{
	RECT rect;
	m_Display.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
}


void CWBCFinderDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRGNList(&m_RGNDataList);

	delete m_RedTIFFData;
	delete m_GreenTIFFData;
	delete m_BlueTIFFData;
	CDialogEx::OnCancel();
}


void CWBCFinderDlg::OnBnClickedLoad()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(FALSE);
	const CString LEICA_RED_FILEPREFIX = _T("TRITC-Rhoadmine Selection");
	const CString LEICA_GREEN_FILEPREFIX = _T("FITC Selection");
	const CString LEICA_BLUE_FILEPREFIX = _T("DAPI Selection");
	const CString ZEISS_RED_POSTFIX = _T("_c3_ORG.tif");
	const CString ZEISS_GREEN_POSTFIX = _T("_c4_ORG.tif");
	const CString ZEISS_BLUE_POSTFIX = _T("_c1_ORG.tif");
	m_SampleName = _T("");
	FreeRGNList(&m_RGNDataList);
	m_TotalHits = 0;
	m_Status = _T("Loading Image, Please wait...");
	UpdateData(FALSE);

	CString message;

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("TIFF files (*.tif)|*.tif"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		WCHAR *ch1 = _T("\\");
		int index1 = filename.ReverseFind(*ch1);
		if (index1 > -1)
		{
			CString pathname = filename.Mid(0, index1);
			index1 = pathname.ReverseFind(*ch1);
			if (index1 > -1)
			{
				m_SampleName = pathname.Mid(index1+1);
			}
		}
		UpdateData(FALSE);
		CString m_ImageFilename = dlg.GetFileName();
		if (m_ImageFilename.Find(_T(".tif")) > 0)
		{
			bool loaded = false;
			int index = m_ImageFilename.Find(LEICA_RED_FILEPREFIX, 0);
			if (index == -1)
			{
				index = m_ImageFilename.Find(ZEISS_RED_POSTFIX, 0);
			}
			if (index == -1)
			{
				message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), m_ImageFilename, LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
				AfxMessageBox(message);
				return;
			}
			bool ret = m_RedTIFFData->LoadRawTIFFFile(filename);
			if (!ret)
			{
				message.Format(_T("Failed to load %s"), m_ImageFilename);
				AfxMessageBox(message);
				return;
			}
			else
			{
				message.Format(_T("Red TIFF File %s loaded successfully, Red CPI=%d"), filename, m_RedTIFFData->GetCPI());
				m_Log.Message(message);
				index = m_ImageFilename.Find(LEICA_RED_FILEPREFIX, 0);
				if (index == -1)
				{
					int postfixIndex = filename.Find(ZEISS_RED_POSTFIX);
					if (postfixIndex == -1)
					{
						message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), m_ImageFilename, LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
						AfxMessageBox(message);
						return;
					}
					else
					{
						CString samplePathName = filename.Mid(0, postfixIndex);
						postfixIndex = m_ImageFilename.Find(ZEISS_RED_POSTFIX);
						CString sampleName = m_ImageFilename.Mid(0, postfixIndex);
						m_ImageFilename = sampleName;
						CString filename2 = samplePathName + ZEISS_GREEN_POSTFIX;
						BOOL ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
						if (ret)
						{
							message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename, m_GreenTIFFData->GetCPI());
							m_Log.Message(message);
							filename2 = samplePathName + ZEISS_BLUE_POSTFIX;
							BOOL ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
							if (ret)
							{
								message.Format(_T("Blue TIFF File %s loaded successfully, Green CPI=%d"), filename, m_BlueTIFFData->GetCPI());
								m_Log.Message(message);
								message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
								m_CTCParams.LoadCTCParams(message);
								m_Status = _T("Image Loading Completed");
								UpdateData(FALSE);
								loaded = true;
							}
							else
							{
								message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
								AfxMessageBox(message);
								return;
							}
						}
						else
						{
							message.Format(_T("Failed to load Green TIFF File %s"), filename2);
							AfxMessageBox(message);
							return;
						}
					}
				}
				else
				{
					CString postfix = m_ImageFilename.Mid(LEICA_RED_FILEPREFIX.GetLength());
					index = filename.Find(LEICA_RED_FILEPREFIX, 0);
					CString pathname = filename.Mid(0, index);
					CString filename2;
					filename2.Format(_T("%s%s%s"), pathname, LEICA_GREEN_FILEPREFIX, postfix);
					ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
					if (!ret)
					{
						message.Format(_T("Failed to load Green TIFF File %s"), filename2);
						AfxMessageBox(message);
						return;
					}
					else
					{
						message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
						m_Log.Message(message);
						pathname = filename.Mid(0, index);
						filename2.Format(_T("%s%s%s"), pathname, LEICA_BLUE_FILEPREFIX, postfix);
						ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
						if (!ret)
						{
							message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
							AfxMessageBox(message);
							return;
						}
						else
						{
							message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
							m_Log.Message(message);
							message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
							m_CTCParams.LoadCTCParams(message);
							m_Status = _T("Image Loading Completed");
							UpdateData(FALSE);
							loaded = true;
							index = postfix.Find(_T(".tif"));
							m_ImageFilename = postfix.Mid(0, index);
						}
					}
				}
			}

			if (loaded)
			{
				CFileDialog dlg1(TRUE,    // open
					NULL,    // no default extension
					NULL,    // no initial file name
					OFN_FILEMUSTEXIST
					| OFN_HIDEREADONLY,
					_T("Region files (*.rgn)|*.rgn"), NULL, 0, TRUE);
				if (dlg1.DoModal() == IDOK)
				{
					CString filename1 = dlg1.GetPathName();
					m_ImageFilename = dlg1.GetFileName();
					UpdateData(FALSE);
					int ctcCount = LoadRegionData(filename1, &m_RGNDataList);
					if (ctcCount > 0)
					{
						m_TotalHits = (int)m_RGNDataList.size();
						m_Status.Format(_T("Loaded Sample %s Region Data with %d Cells and %d CTCs"), m_SampleName, m_TotalHits, ctcCount);
						UpdateData(FALSE);
						CopyToRGBImage();
						UpdateImageDisplay();
					}
					else
					{
						m_Status.Format(_T("Failed to Load Sample %s Region Data"), m_SampleName);
						UpdateData(FALSE);
					}
				}
			}
		}
	}
	btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(TRUE);
}

void CWBCFinderDlg::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	(*rgnList).clear();
}

void CWBCFinderDlg::CopyToRGBImage()
{
	if ((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && ((m_Image.GetHeight() == NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT)))
	{
		BYTE *pCursor = (BYTE*)m_Image.GetBits();
		int nPitch = m_Image.GetPitch();
		for (int ImageLocationIndex = 0, startIndex=0; ImageLocationIndex < NUM_IMAGES; ImageLocationIndex++, startIndex++)
		{
			for (startIndex; startIndex < m_TotalHits; startIndex++)
				if (m_RGNDataList[startIndex]->GetColorCode() == CTC2)
					break;
			if (startIndex < m_TotalHits)
			{
				CRGNData *rgnPtr = m_RGNDataList[startIndex];
				int regionWidth = rgnPtr->GetWidth();
				int regionHeight = rgnPtr->GetHeight();

				unsigned short *pBlueBuffer = rgnPtr->GetImage(BLUE_COLOR);
				unsigned short *pGreenBuffer = rgnPtr->GetImage(GREEN_COLOR);
				unsigned short *pRedBuffer = rgnPtr->GetImage(RED_COLOR);

				int blueMax = rgnPtr->GetContrast(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR);
				int redMax = rgnPtr->GetContrast(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR);
				int greenMax = rgnPtr->GetContrast(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR);

				BYTE bluePixelValue = 0;
				BYTE greenPixelValue = 0;
				BYTE redPixelValue = 0;

				for (int y = 0; y < regionHeight; y++)
				{
					for (int x = 0; x < regionWidth; x++)
					{
						bluePixelValue = GetContrastEnhancedByte(*pBlueBuffer++, blueMax, rgnPtr->GetCutoff(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR));
						greenPixelValue = GetContrastEnhancedByte(*pGreenBuffer++, greenMax, rgnPtr->GetCutoff(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR));
						redPixelValue = GetContrastEnhancedByte(*pRedBuffer++, redMax, rgnPtr->GetCutoff(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR));
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x)] = bluePixelValue;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 1] = greenPixelValue;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 2] = redPixelValue;
					}
				}
			}
			else
			{
				for (int y = 0; y < SAMPLEFRAMEHEIGHT; y++)
				{
					for (int x = 0; x < SAMPLEFRAMEWIDTH; x++)
					{
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x)] = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 1] = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 2] = (BYTE)0;
					}
				}
			}
		}
	}
}

BYTE CWBCFinderDlg::GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff)
{
	BYTE result = 0;
	int value1 = value - cutoff;
	int maxValue = (int)contrast;
	maxValue -= cutoff;
	if (maxValue < 255)
		maxValue = 255;

	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);;
	}
	return result;
}

int CWBCFinderDlg::LoadRegionData(CString filename, vector<CRGNData *> *wbcList)
{
	int regionCount = 0;
	const CString TAG1 = _T("0 1, 1 ");
	const CString TAG2 = _T(", 2 ");
	const CString TAG3 = _T(", ");

	vector<CRGNData *> *list = wbcList;
	FreeRGNList(list);

	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			int index = textline.Find(TAG1);
			if (index > -1)
			{
				textline = textline.Mid(TAG1.GetLength());
				index = textline.Find(TAG2);
				if (index > -1)
				{
					unsigned int color = _wtoi(textline.Mid(0, index));
					textline = textline.Mid(index + TAG2.GetLength());
					index = textline.Find(_T(" "));
					if (index > -1)
					{
						int x0 = _wtoi(textline.Mid(0, index));
						textline = textline.Mid(index + 1);
						index = textline.Find(TAG3);
						if (index > -1)
						{
							int y0 = _wtoi(textline.Mid(0, index));
							CRGNData *data = new CRGNData(x0, y0, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT);
							data->SetColorCode(color);
							data->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
							data->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
							data->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
							data->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
							data->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
							data->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
							data->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
							data->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
							data->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
							unsigned short *redImg = new unsigned short[SAMPLEFRAMEWIDTH * SAMPLEFRAMEHEIGHT];
							m_RedTIFFData->GetImageRegion(x0, y0, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT, redImg);
							unsigned short *greenImg = new unsigned short[SAMPLEFRAMEWIDTH * SAMPLEFRAMEHEIGHT];
							m_GreenTIFFData->GetImageRegion(x0, y0, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT, greenImg);
							unsigned short *blueImg = new unsigned short[SAMPLEFRAMEWIDTH * SAMPLEFRAMEHEIGHT];
							m_BlueTIFFData->GetImageRegion(x0, y0, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT, blueImg);
							data->SetImages(redImg, greenImg, blueImg);
							list->push_back(data);
							regionCount++;
							continue;
						}
						else
						{
							break;
						}
					}
					else
					{
						break;
					}
				}
				else
				{
					break;
				}
			}
			else
			{
				break;
			}
		}
		theFile.Close();
	}
	return regionCount;
}

void CWBCFinderDlg::OnBnClickedSavergn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_SAVEHIT);
	btn->EnableWindow(FALSE);
	int count = 0;
	for (int i = 0; i < (int)m_RGNDataList.size(); i++)
	{
		unsigned int color = m_RGNDataList[i]->GetColorCode();
		if (color == CTC2)
			count++;
	}
	if (count > 0)
	{
		CString message;
		CString filenameForSave;
		filenameForSave.Format(_T("C:\\Users\\Xiaoming\\Documents\\HitFiles\\%sCTC.hit"), m_SampleName);
		CString filenameToDisplay;
		filenameToDisplay.Format(_T("%sCTC.hit"), m_SampleName);
		CFile theFile;
		if (theFile.Open(filenameForSave, CFile::modeCreate | CFile::modeWrite))
		{
			CArchive archive(&theFile, CArchive::store);
			archive << (int)3; // Version Number	
			archive << count;
			if (m_RedTIFFData->IsZeissData())
				archive << 1;
			else
				archive << 0;
			archive << m_RedTIFFData->GetCPI();
			archive << m_GreenTIFFData->GetCPI();
			archive << m_BlueTIFFData->GetCPI();
			for (int i = 0; i < (int)m_RGNDataList.size(); i++)
			{
				CSingleRegionImageData *data = new CSingleRegionImageData();
				CRGNData *ptr = m_RGNDataList[i];
				unsigned int color = ptr->GetColorCode();
				data->m_HitIndex = i;
				data->m_ColorCode = color;
				if (color == CTC2)
				{
					int x0, y0;
					ptr->GetPosition(&x0, &y0);
					data->m_RegionX0Pos = x0;
					data->m_RegionY0Pos = y0;
					data->m_RegionWidth = ptr->GetWidth();
					data->m_RegionHeight = ptr->GetHeight();
					data->m_RedRegionImage = ptr->GetImage(RED_COLOR);
					data->m_GreenRegionImage = ptr->GetImage(GREEN_COLOR);
					data->m_BlueRegionImage = ptr->GetImage(BLUE_COLOR);
					data->Serialize(archive);
					data->m_RedRegionImage = NULL;
					data->m_GreenRegionImage = NULL;
					data->m_BlueRegionImage = NULL;
				}
				delete data;
			}
			archive.Close();
			theFile.Close();
			message.Format(_T("Saved %d Hits to Hit Archive File %s"), count, filenameForSave);
			m_Log.Message(message);
			m_Status.Format(_T("Saved %d CTC Regions to %s file"), count, filenameToDisplay);
			UpdateData(FALSE);
		}
		else
		{
			message.Format(_T("Failed to open %s"), filenameForSave);
			m_Log.Message(message);
		}
	}
	else
	{
		m_Status.Format(_T("# of CTC Regions=0"));
		UpdateData(FALSE);
	}
	btn->EnableWindow(TRUE);
}

LRESULT CWBCFinderDlg::OnMyMessage(WPARAM wparam, LPARAM lparam)
{
	UpdateData(FALSE);
	return 0;
}

void CWBCFinderDlg::EnableButtons(BOOL enable)
{
	CButton *btn = (CButton *)GetDlgItem(IDC_BATCH);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	btn->EnableWindow(enable);
}

void CWBCFinderDlg::OnBnClickedBatch()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);

	HANDLE thread = ::CreateThread(NULL, 0, SaveHitBatchRun, this, CREATE_SUSPENDED, NULL);
	CString message;

	if (!thread)
	{
		AfxMessageBox(_T("Fail to create thread for batch run"));
		message.Format(_T("Failed to creat a thread for batch run"));
		m_Log.Message(message);
		EnableButtons(TRUE);
		return;
	}

	::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
	::ResumeThread(thread);
	message.Format(_T("Launched a thread for batch run"));
	m_Log.Message(message);
	m_Status.Format(_T("Launched a thread for batch run"));
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
}

DWORD WINAPI SaveHitBatchRun(LPVOID param)
{
	CStdioFile theFile;
	CWBCFinderDlg *ptr = (CWBCFinderDlg *)param;
	CString message;
	bool success = true;

	if (theFile.Open(_T("CellFinderBatch.txt"), CFile::modeRead | CFile::typeText))
	{
		CString textline;
		int lineIdx = 0;
		while (theFile.ReadString(textline))
		{
			lineIdx++;
			int index = textline.Find(_T(","));
			if (index == -1)
			{
				ptr->m_Status.Format(_T("CellFinderBatch.txt Line#%d Error: Fail to read pathname"), lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("CellFinderBatch.txt Line#%d Error: Fail to read pathname"), lineIdx);
				ptr->m_Log.Message(message);
				success = false;
				break;
			}
			CString pathname = textline.Mid(0, index);
			textline = textline.Mid(index + 1);
			index = textline.Find(_T(","));
			if (index == -1)
			{
				ptr->m_Status.Format(_T("CellFinderBatch.txt Line#%d Error: Fail to read image filename"), lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("CellFinderBatch.txt Line#%d Error: Fail to read image filename"), lineIdx);
				ptr->m_Log.Message(message);
				success = false;
				break;
			}
			CString imagefilename = textline.Mid(0, index);
			CString regionfilename = textline.Mid(index + 1);
			WCHAR *ch1 = _T("\\");
			index = pathname.ReverseFind(*ch1);
			CString sampleName = pathname.Mid(index + 1);
			ptr->m_Status.Format(_T("Started to save Hit File for Sample No.%d %s"), lineIdx, sampleName);
			::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			bool ret = ptr->LoadImagesAndSaveHitFile(sampleName, pathname, imagefilename, regionfilename);
			if (ret)
			{
				ptr->m_Status.Format(_T("Saved Hit File for Sample No.%d successfully"), lineIdx, sampleName);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("Saved Hit File for Sample No.%d successfully"), lineIdx, sampleName);
				ptr->m_Log.Message(message);
				success = true;
			}
			else
			{
				ptr->m_Status.Format(_T("Failed to save Hit File for Sample No.%d"), lineIdx, sampleName);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("Failed to save Hit File for Sample No.%d"), lineIdx, sampleName);
				ptr->m_Log.Message(message);
				success = false;
				break;
			}
		}
		theFile.Close();
	}

	ptr->EnableButtons(TRUE);
	return 0;
}

bool CWBCFinderDlg::LoadImagesAndSaveHitFile(CString sampleName, CString imagepathname, CString imagefilename, CString regionfilename)
{
	const CString LEICA_RED_FILEPREFIX = _T("TRITC-Rhoadmine Selection");
	const CString LEICA_GREEN_FILEPREFIX = _T("FITC Selection");
	const CString LEICA_BLUE_FILEPREFIX = _T("DAPI Selection");
	const CString ZEISS_RED_POSTFIX = _T("_c3_ORG.tif");
	const CString ZEISS_GREEN_POSTFIX = _T("_c4_ORG.tif");
	const CString ZEISS_BLUE_POSTFIX = _T("_c1_ORG.tif");
	FreeRGNList(&m_RGNDataList);
	CString message;
	CString filename = imagepathname + _T("\\") + imagefilename;
	bool loaded = false;
	int index = imagefilename.Find(LEICA_RED_FILEPREFIX, 0);
	if (index == -1)
	{
		index = imagefilename.Find(ZEISS_RED_POSTFIX, 0);
	}
	if (index == -1)
	{
		message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), imagefilename, LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
		m_Log.Message(message);
		return false;
	}
	bool ret = m_RedTIFFData->LoadRawTIFFFile(filename);
	if (!ret)
	{
		message.Format(_T("Failed to load %s"), filename);
		m_Log.Message(message);
		filename = imagepathname + _T("\\") + LEICA_RED_FILEPREFIX + sampleName + _T(".tif");
		imagefilename = LEICA_RED_FILEPREFIX + sampleName + _T(".tif");
		ret = m_RedTIFFData->LoadRawTIFFFile(filename);
		if (!ret)
		{
			message.Format(_T("Failed to load %s"), filename);
			m_Log.Message(message);
			return false;
		}
	}
	
	if (ret)
	{
		message.Format(_T("Red TIFF File %s loaded successfully, Red CPI=%d"), filename, m_RedTIFFData->GetCPI());
		m_Log.Message(message);
		index = filename.Find(LEICA_RED_FILEPREFIX, 0);
		if (index == -1)
		{
			int postfixIndex = filename.Find(ZEISS_RED_POSTFIX);
			if (postfixIndex == -1)
			{
				message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), imagefilename, LEICA_RED_FILEPREFIX, ZEISS_RED_POSTFIX);
				m_Log.Message(message);
				return false;
			}
			else
			{
				CString samplePathName = filename.Mid(0, postfixIndex);
				CString filename2 = samplePathName + ZEISS_GREEN_POSTFIX;
				ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
				if (ret)
				{
					message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename, m_GreenTIFFData->GetCPI());
					m_Log.Message(message);
					filename2 = samplePathName + ZEISS_BLUE_POSTFIX;
					ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
					if (ret)
					{
						message.Format(_T("Blue TIFF File %s loaded successfully, Green CPI=%d"), filename, m_BlueTIFFData->GetCPI());
						m_Log.Message(message);
						message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
						m_CTCParams.LoadCTCParams(message);
						loaded = true;
					}
					else
					{
						message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
						m_Log.Message(message);
						return false;
					}
				}
				else
				{
					message.Format(_T("Failed to load Green TIFF File %s"), filename2);
					m_Log.Message(message);
					return false;
				}
			}
		}
		else
		{
			CString postfix = imagefilename.Mid(LEICA_RED_FILEPREFIX.GetLength());
			index = filename.Find(LEICA_RED_FILEPREFIX, 0);
			CString pathname = filename.Mid(0, index);
			CString filename2;
			filename2.Format(_T("%s%s%s"), pathname, LEICA_GREEN_FILEPREFIX, postfix);
			ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
			if (!ret)
			{
				message.Format(_T("Failed to load Green TIFF File %s"), filename2);
				m_Log.Message(message);
				return false;
			}
			else
			{
				message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
				m_Log.Message(message);
				filename2.Format(_T("%s%s%s"), pathname, LEICA_BLUE_FILEPREFIX, postfix);
				ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
				if (!ret)
				{
					message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
					m_Log.Message(message);
					return false;
				}
				else
				{
					message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
					m_Log.Message(message);
					message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
					m_CTCParams.LoadCTCParams(message);
					loaded = true;
				}
			}
		}
	}

	ret = false;
	if (loaded)
	{
		ret = true;
		CString filename1 = imagepathname + _T("\\") + regionfilename;
		int regionCount = LoadRegionData(filename1, &m_RGNDataList);
		if (regionCount > 0)
		{
			message.Format(_T("Load Region File %s Successfully, # Regions=%d"), filename1, regionCount);
			m_Log.Message(message);
			int count = 0;
			for (int i = 0; i < (int)m_RGNDataList.size(); i++)
			{
				unsigned int color = m_RGNDataList[i]->GetColorCode();
				if ((color == CTC) || (color == CTC2))
					count++;
			}
			if (count > 0)
			{
				CString message;
				CString filenameForSave;
				filenameForSave.Format(_T("C:\\Users\\Xiaoming\\Documents\\CTCHitFiles\\%sHIT.hit"), sampleName);
				CFile theFile;
				if (theFile.Open(filenameForSave, CFile::modeCreate | CFile::modeWrite))
				{
					CArchive archive(&theFile, CArchive::store);
					archive << (int)3; // Version Number	
					archive << count;
					if (m_RedTIFFData->IsZeissData())
						archive << 1;
					else
						archive << 0;
					archive << m_RedTIFFData->GetCPI();
					archive << m_GreenTIFFData->GetCPI();
					archive << m_BlueTIFFData->GetCPI();
					for (int i = 0; i < (int)m_RGNDataList.size(); i++)
					{
						CSingleRegionImageData *data = new CSingleRegionImageData();
						CRGNData *ptr = m_RGNDataList[i];
						unsigned int color = ptr->GetColorCode();
						data->m_HitIndex = i;
						data->m_ColorCode = color;
						if ((color == CTC) || (color == CTC2))
						{
							int x0, y0;
							ptr->GetPosition(&x0, &y0);
							data->m_RegionX0Pos = x0;
							data->m_RegionY0Pos = y0;
							data->m_RegionWidth = ptr->GetWidth();
							data->m_RegionHeight = ptr->GetHeight();
							data->m_RedRegionImage = ptr->GetImage(RED_COLOR);
							data->m_GreenRegionImage = ptr->GetImage(GREEN_COLOR);
							data->m_BlueRegionImage = ptr->GetImage(BLUE_COLOR);
							data->Serialize(archive);
							data->m_RedRegionImage = NULL;
							data->m_GreenRegionImage = NULL;
							data->m_BlueRegionImage = NULL;
						}
						delete data;
					}
					archive.Close();
					theFile.Close();
					message.Format(_T("Saved %d Hits to Hit Archive File %s"), count, filenameForSave);
					m_Log.Message(message);
				}
				else
				{
					message.Format(_T("Failed to open %s"), filenameForSave);
					m_Log.Message(message);
					ret = false;
				}
			}
			else
				ret = false;
		}
		else
		{
			message.Format(_T("Failed to load Region File %s"), filename1);
			m_Log.Message(message);
			ret = false;
		}
	}

	return ret;
}