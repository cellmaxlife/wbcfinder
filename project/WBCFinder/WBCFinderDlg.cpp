﻿
// WBCFinderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "WBCFinder.h"
#include "WBCFinderDlg.h"
#include "afxdialogex.h"
#include "VersionNumber.h"
#include "SingleRegionImageData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CWBCFinderDlg dialog

#define NUM_IMAGE_COLUMNS	4
#define NUM_IMAGE_ROWS		4
#define NUM_IMAGES			16
#define WM_MY_MESSAGE (WM_USER+1001)
#define WM_ENABLE_BUTTONS (WM_USER+1002)
#define MAXINT_12BIT 4096
#define MAXINT_14BIT 16384

static DWORD WINAPI WBCScreenOperation(LPVOID param);
static DWORD WINAPI WBCBatchRun(LPVOID param);

CWBCFinderDlg::CWBCFinderDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CWBCFinderDlg::IDD, pParent)
	, m_Status(_T(""))
	, m_ImageFilename(_T(""))
	, m_HitIndex(0)
	, m_TotalHits(0)
	, m_AverageCD45(0)
	, m_FullImagePathname(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	Leica_Red_Prefix = _T("");
	Leica_Green_Prefix = _T("");
	Leica_Blue_Prefix = _T("");
	Zeiss_Red_Postfix = _T("");
	Zeiss_Green_Postfix = _T("");
	Zeiss_Blue_Postfix = _T("");
}

void CWBCFinderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DISPLAY, m_Display);
	DDX_Text(pDX, IDC_STATUS, m_Status);
	DDX_Text(pDX, IDC_IMAGEFILE, m_ImageFilename);
	DDX_Text(pDX, IDC_HITIDX, m_HitIndex);
	DDX_Text(pDX, IDC_TOTALHITS, m_TotalHits);
	DDX_Text(pDX, IDC_CD45, m_AverageCD45);
	DDX_Control(pDX, IDC_SHOW, m_ShowBoundary);
	DDX_Control(pDX, IDC_BLUE, m_Blue);
	DDX_Control(pDX, IDC_GREEN, m_Green);
	DDX_Control(pDX, IDC_REDCOLOR, m_Red);
	DDX_Control(pDX, IDC_LOADONE, m_LoadOneImageATime);
}

BEGIN_MESSAGE_MAP(CWBCFinderDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CWBCFinderDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_LOAD, &CWBCFinderDlg::OnBnClickedLoad)
	ON_BN_CLICKED(IDC_NEXTHIT, &CWBCFinderDlg::OnBnClickedNexthit)
	ON_BN_CLICKED(IDC_PREVHIT, &CWBCFinderDlg::OnBnClickedPrevhit)
	ON_BN_CLICKED(IDC_GOTOHIT, &CWBCFinderDlg::OnBnClickedGotohit)
	ON_MESSAGE(WM_MY_MESSAGE, OnMyMessage)
	ON_BN_CLICKED(IDC_SAVERGN, &CWBCFinderDlg::OnBnClickedSavergn)
	ON_BN_CLICKED(IDC_REDCOLOR, &CWBCFinderDlg::OnBnClickedRedcolor)
	ON_BN_CLICKED(IDC_GREEN, &CWBCFinderDlg::OnBnClickedGreen)
	ON_BN_CLICKED(IDC_BLUE, &CWBCFinderDlg::OnBnClickedBlue)
	ON_BN_CLICKED(IDC_SHOW, &CWBCFinderDlg::OnBnClickedShow)
	ON_BN_CLICKED(IDC_BATCHRUN, &CWBCFinderDlg::OnBnClickedBatchrun)
	ON_MESSAGE(WM_ENABLE_BUTTONS, OnEnableButtons)
END_MESSAGE_MAP()


// CWBCFinderDlg message handlers

BOOL CWBCFinderDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CString version;
	version.Format(_T("WBCFinder(%s)"), CELLMAPPLUS_VERSION);
	m_Log.NewLog(version);
	m_HitFinder.m_Log = &m_Log;
	SetWindowText(version);
	version.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
	m_CTCParams.LoadCTCParams(version);
	LoadDefaultSettings();
	m_RedTIFFData = new CSingleChannelTIFFData();
	m_GreenTIFFData = new CSingleChannelTIFFData();
	m_BlueTIFFData = new CSingleChannelTIFFData();
	m_Status = _T("Please Load Image");
	UpdateData(FALSE);
	m_Red.SetCheck(BST_CHECKED);
	m_Green.SetCheck(BST_CHECKED);
	m_Blue.SetCheck(BST_CHECKED);
	m_ShowBoundary.SetCheck(BST_CHECKED);
	m_LoadOneImageATime.SetCheck(BST_CHECKED);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CWBCFinderDlg::LoadDefaultSettings()
{
	CStdioFile theFile;
	if (theFile.Open(_T(".\\DefaultSettings.txt"), CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			if (textline.Find(_T("Leica_Red_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Red_Prefix=");
				Leica_Red_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Leica_Green_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Green_Prefix=");
				Leica_Green_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Leica_Blue_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Blue_Prefix=");
				Leica_Blue_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Red_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Red_Postfix=");
				Zeiss_Red_Postfix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Green_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Green_Postfix=");
				Zeiss_Green_Postfix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Blue_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Blue_Postfix=");
				Zeiss_Blue_Postfix = textline.Mid(tag.GetLength());
			}
		}
		theFile.Close();
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CWBCFinderDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		PaintImages();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CWBCFinderDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CWBCFinderDlg::PaintImages()
{
	if (m_Image == NULL)
		return;
	else if ((m_Image.GetWidth() == m_RedTIFFData->GetSubsampledWidth()) && (m_Image.GetHeight() == m_RedTIFFData->GetSubsampledHeight()))
	{
		CPaintDC dc(&m_Display);
		CRect rect;
		m_Display.GetClientRect(&rect);
		dc.SetStretchBltMode(HALFTONE);
		m_Image.StretchBlt(dc.m_hDC, rect);
	}
	else if ((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && ((m_Image.GetHeight() == NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT)))
	{
		CPaintDC dc(&m_Display);
		CRect rect;
		m_Display.GetClientRect(&rect);
		dc.SetStretchBltMode(HALFTONE);
		m_Image.StretchBlt(dc.m_hDC, rect);
		CDC* pDC = m_Display.GetDC();
		CPen CursorPen(PS_SOLID, 3, RGB(255, 255, 255));
		CPen *pOldPen = pDC->SelectObject(&CursorPen);
		int blockWidth = (rect.right - rect.left + 1) / NUM_IMAGE_COLUMNS;
		for (int i = 1; i < NUM_IMAGE_COLUMNS; i++)
		{
			pDC->MoveTo(rect.left + i * blockWidth, rect.top);
			pDC->LineTo(rect.left + i * blockWidth, rect.bottom);
		}
		int blockHeight = (rect.bottom - rect.top + 1) / NUM_IMAGE_ROWS;
		for (int i = 1; i < NUM_IMAGE_ROWS; i++)
		{
			pDC->MoveTo(rect.left, rect.top + i * blockHeight);
			pDC->LineTo(rect.right, rect.top + i * blockHeight);
		}
		pDC->SelectObject(pOldPen);
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextAlign(TA_LEFT);
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		for (int i = 0; i < NUM_IMAGES; i++, startIndex++)
		{
			if (startIndex < m_TotalHits)
			{
				int ii = i / NUM_IMAGE_COLUMNS;
				int jj = i % NUM_IMAGE_COLUMNS;
				CRect rect1;
				rect1.left = rect.left + jj * blockWidth + 10;
				rect1.top = rect.top + ii * blockHeight + 10;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				CString label;
				label.Format(_T("%d"), (startIndex + 1));
				if (m_RGNDataList[startIndex]->GetBlobData(GREEN_COLOR)->size() > 0)
					pDC->SetTextColor(RGB(0, 255, 0));
				else
					pDC->SetTextColor(RGB(255, 255, 255));
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
			}
		}
	}
}

void CWBCFinderDlg::UpdateImageDisplay()
{
	RECT rect;
	m_Display.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
}


void CWBCFinderDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRGNList(&m_RGNDataList);

	delete m_RedTIFFData;
	delete m_GreenTIFFData;
	delete m_BlueTIFFData;
	CDialogEx::OnCancel();
}


void CWBCFinderDlg::OnBnClickedLoad()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(FALSE);
	m_FullImagePathname = _T("");
	m_ImageFilename = _T("");
	FreeRGNList(&m_RGNDataList);
	m_TotalHits = 0;
	m_HitIndex = 0;
	if (m_Image != NULL)
		m_Image.Destroy();
	if (m_LoadOneImageATime.GetCheck() == BST_UNCHECKED)
	{
		m_Status = _T("Loading and Processing Image, Please wait...");
		UpdateData(FALSE);

		CString message;

		CFileDialog dlg(TRUE,    // open
			NULL,    // no default extension
			NULL,    // no initial file name
			OFN_FILEMUSTEXIST
			| OFN_HIDEREADONLY,
			_T("TIFF or Hit files (*.tif, *.hit)|*.tif; *.hit"), NULL, 0, TRUE);
		if (dlg.DoModal() == IDOK)
		{
			CString filename = dlg.GetPathName();
			m_FullImagePathname = filename;
			m_ImageFilename = dlg.GetFileName();
			UpdateData(FALSE);
			if (m_ImageFilename.Find(_T(".tif")) > 0)
			{
				bool loaded = false;
				int index = m_ImageFilename.Find(Leica_Red_Prefix, 0);
				if (index == -1)
				{
					index = m_ImageFilename.Find(Zeiss_Red_Postfix, 0);
				}
				if (index == -1)
				{
					message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), m_ImageFilename, Leica_Red_Prefix, Zeiss_Red_Postfix);
					AfxMessageBox(message);
					btn = (CButton *)GetDlgItem(IDC_LOAD);
					btn->EnableWindow(TRUE);
					m_Status = _T("Failed to load red channel image");
					UpdateData(FALSE);
					return;
				}
				bool ret = m_RedTIFFData->LoadRawTIFFFile(filename);
				if (!ret)
				{
					message.Format(_T("Failed to load %s"), m_ImageFilename);
					AfxMessageBox(message);
					btn = (CButton *)GetDlgItem(IDC_LOAD);
					btn->EnableWindow(TRUE);
					m_Status = _T("Failed to load red channel image");
					UpdateData(FALSE);
					return;
				}
				else
				{
					message.Format(_T("Red TIFF File %s loaded successfully, Red CPI=%d"), filename, m_RedTIFFData->GetCPI());
					m_Log.Message(message);
					index = m_ImageFilename.Find(Leica_Red_Prefix, 0);
					if (index == -1)
					{
						int postfixIndex = filename.Find(Zeiss_Red_Postfix);
						if (postfixIndex == -1)
						{
							message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), m_ImageFilename, Leica_Red_Prefix, Zeiss_Red_Postfix);
							AfxMessageBox(message);
							btn = (CButton *)GetDlgItem(IDC_LOAD);
							btn->EnableWindow(TRUE);
							m_Status = _T("Failed to load green channel image");
							UpdateData(FALSE);
							return;
						}
						else
						{
							CString samplePathName = filename.Mid(0, postfixIndex);
							postfixIndex = m_ImageFilename.Find(Zeiss_Red_Postfix);
							CString sampleName = m_ImageFilename.Mid(0, postfixIndex);
							m_ImageFilename = sampleName;
							CString filename2 = samplePathName + Zeiss_Green_Postfix;
							BOOL ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
							if (ret)
							{
								message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename, m_GreenTIFFData->GetCPI());
								m_Log.Message(message);
								filename2 = samplePathName + Zeiss_Blue_Postfix;
								BOOL ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
								if (ret)
								{
									message.Format(_T("Blue TIFF File %s loaded successfully, Green CPI=%d"), filename, m_BlueTIFFData->GetCPI());
									m_Log.Message(message);
									message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
									m_CTCParams.LoadCTCParams(message);
									m_Status = _T("Image Loading Completed");
									UpdateData(FALSE);
									loaded = true;
								}
								else
								{
									message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
									AfxMessageBox(message);
									btn = (CButton *)GetDlgItem(IDC_LOAD);
									btn->EnableWindow(TRUE);
									m_Status = _T("Failed to load blue channel image");
									UpdateData(FALSE);
									return;
								}
							}
							else
							{
								message.Format(_T("Failed to load Green TIFF File %s"), filename2);
								AfxMessageBox(message);
								btn = (CButton *)GetDlgItem(IDC_LOAD);
								btn->EnableWindow(TRUE);
								m_Status = _T("Failed to load green channel image");
								UpdateData(FALSE);
								return;
							}
						}
					}
					else
					{
						CString postfix = m_ImageFilename.Mid(Leica_Red_Prefix.GetLength());
						index = filename.Find(Leica_Red_Prefix, 0);
						CString pathname = filename.Mid(0, index);
						CString filename2;
						filename2.Format(_T("%s%s%s"), pathname, Leica_Green_Prefix, postfix);
						ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
						if (!ret)
						{
							message.Format(_T("Failed to load Green TIFF File %s"), filename2);
							AfxMessageBox(message);
							btn = (CButton *)GetDlgItem(IDC_LOAD);
							btn->EnableWindow(TRUE);
							m_Status = _T("Failed to load green channel image");
							UpdateData(FALSE);
							return;
						}
						else
						{
							message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
							m_Log.Message(message);
							pathname = filename.Mid(0, index);
							filename2.Format(_T("%s%s%s"), pathname, Leica_Blue_Prefix, postfix);
							ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
							if (!ret)
							{
								message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
								AfxMessageBox(message);
								btn = (CButton *)GetDlgItem(IDC_LOAD);
								btn->EnableWindow(TRUE);
								m_Status = _T("Failed to load blue channel image");
								UpdateData(FALSE);
								return;
							}
							else
							{
								message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
								m_Log.Message(message);
								message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
								m_CTCParams.LoadCTCParams(message);
								m_Status = _T("Image Loading Completed");
								UpdateData(FALSE);
								loaded = true;
								index = postfix.Find(_T(".tif"));
								m_ImageFilename = postfix.Mid(0, index);
							}
						}
					}
				}

				if (loaded)
				{
					int width = m_RedTIFFData->GetSubsampledWidth();
					int height = m_RedTIFFData->GetSubsampledHeight();
					m_Image.Create(width, -height, 24);
					CopyToRGBImage();
					UpdateImageDisplay();

					HANDLE thread = ::CreateThread(NULL, 0, WBCScreenOperation, this, CREATE_SUSPENDED, NULL);

					if (!thread)
					{
						AfxMessageBox(_T("Fail to create thread for screening"));
						message.Format(_T("Failed to creat a thread for screening"));
						m_Log.Message(message);
						btn = (CButton *)GetDlgItem(IDC_LOAD);
						btn->EnableWindow(TRUE);
						return;
					}

					::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
					::ResumeThread(thread);
					message.Format(_T("Launched a thread for screening"));
					m_Log.Message(message);
					m_Status.Format(_T("Started Image Screening for WBCs"));
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				}
			}
			else
			{
				CFile theFile;
				if (theFile.Open(filename, CFile::modeRead))
				{
					int CapturedCount = 0;
					CArchive archive(&theFile, CArchive::load);
					int version = 0;
					archive >> version;
					if (version != 3)
					{
						m_Status.Format(_T("Hits Image Data Archive Version Number (=%d) != 3"), version);
						UpdateData(FALSE);
						m_Status.Format(_T("Hit Image Data Archive File %s has a version number (=%d) != 3"), version);
						m_Log.Message(m_Status);
					}
					else
					{
						int totalCount = 0;
						archive >> totalCount;
						if (totalCount > 0)
						{
							int isZeissData;
							archive >> isZeissData;
							if (isZeissData == 0)
								m_ZeissData = false;
							else
								m_ZeissData = true;
							unsigned short CPI_Value;
							archive >> CPI_Value;
							int m_RedIntensity = CPI_Value;
							archive >> CPI_Value;
							int m_GreenIntensity = CPI_Value;
							archive >> CPI_Value;
							int m_BlueIntensity = CPI_Value;
							CString message;
							message.Format(_T("Hit Filename=%s, #ConfirmedWBCs=%d"), m_ImageFilename, totalCount);
							m_Log.Message(message);
							for (int i = 0; i < totalCount; i++)
							{
								CSingleRegionImageData *data = new CSingleRegionImageData();
								data->Serialize(archive);
								CRGNData *ptr = new CRGNData(data->m_RegionX0Pos, data->m_RegionY0Pos, data->m_RegionWidth, data->m_RegionHeight);
								ptr->SetImages(data->m_RedRegionImage, data->m_GreenRegionImage, data->m_BlueRegionImage);
								ptr->m_HitIndex = data->m_HitIndex + 1;
								data->m_RedRegionImage = NULL;
								data->m_GreenRegionImage = NULL;
								data->m_BlueRegionImage = NULL;
								delete data;
								ptr->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
								ptr->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
								ptr->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
								ptr->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
								ptr->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
								ptr->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
								ptr->SetCPI(RED_COLOR, m_RedIntensity);
								ptr->SetCPI(GREEN_COLOR, m_GreenIntensity);
								ptr->SetCPI(BLUE_COLOR, m_BlueIntensity);
								if (version == 3)
								{
									if (m_HitFinder.ProcessOneRegion(&m_CTCParams, ptr, m_ZeissData))
									{
										CapturedCount++;
									}
									else
									{
										message.Format(_T("Region No.%d Not Captured"), i + 1);
										m_Log.Message(message);
									}
									m_RGNDataList.push_back(ptr);
								}
							}
						}
						else
						{
							m_Status.Format(_T("NumHits (=%d) = 0"), totalCount);
							UpdateData(FALSE);
						}
					}
					archive.Close();
					theFile.Close();
					m_TotalHits = (int)m_RGNDataList.size();
					UpdateData(FALSE);
					if (m_TotalHits > 0)
					{
						m_Image.Create(NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH, -(NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT), 24);
						m_HitIndex = 1;
						CopyToRGBImage();
						m_AverageCD45 = GetAverageCD45(&m_RGNDataList);
						UpdateData(FALSE);
						UpdateImageDisplay();
						m_Status.Format(_T("HitFile %s, Total Hits=%d, Captured Hits=%d"), m_ImageFilename, m_TotalHits, CapturedCount);
						UpdateData(FALSE);
					}
					else
					{
						m_Status.Format(_T("Hit File Loaded without finding any WBCs"), m_ImageFilename);
						UpdateData(FALSE);
					}
				}
			}
		}
		btn = (CButton *)GetDlgItem(IDC_LOAD);
		btn->EnableWindow(TRUE);
	}
	else
	{
		m_Status = _T("Loading Red Channel Image, Please wait...");
		UpdateData(FALSE);

		CString message;
		bool loaded = false;

		CFileDialog dlg(TRUE,    // open
			NULL,    // no default extension
			NULL,    // no initial file name
			OFN_FILEMUSTEXIST
			| OFN_HIDEREADONLY,
			_T("Red Channel TIFF files (*.tif)|*.tif"), NULL, 0, TRUE);
		if (dlg.DoModal() == IDOK)
		{
			CString filename = dlg.GetPathName();
			m_FullImagePathname = filename;
			CString sampleFileName = dlg.GetFileName();
			m_ImageFilename = sampleFileName.Mid(0, sampleFileName.Find(_T(".tif")));
			UpdateData(FALSE);
			bool ret = m_RedTIFFData->LoadRawTIFFFile(filename);
			if (!ret)
			{
				message.Format(_T("Failed to load Red Channel Image File - %s"), filename);
				AfxMessageBox(message);
				btn = (CButton *)GetDlgItem(IDC_LOAD);
				btn->EnableWindow(TRUE);
				m_Status = _T("Failed to load red channel image");
				UpdateData(FALSE);
				return;
			}
		}
		else
		{
			btn = (CButton *)GetDlgItem(IDC_LOAD);
			btn->EnableWindow(TRUE);
			m_Status = _T("Failed to load red channel image");
			UpdateData(FALSE);
			return;
		}

		m_Status = _T("Loading Green Channel Image, Please wait...");
		UpdateData(FALSE);

		CFileDialog dlg1(TRUE,    // open
			NULL,    // no default extension
			NULL,    // no initial file name
			OFN_FILEMUSTEXIST
			| OFN_HIDEREADONLY,
			_T("Green Channel TIFF files (*.tif)|*.tif"), NULL, 0, TRUE);
		if (dlg1.DoModal() == IDOK)
		{
			CString filename = dlg1.GetPathName();
			UpdateData(FALSE);
			bool ret = m_GreenTIFFData->LoadRawTIFFFile(filename);
			if (!ret)
			{
				message.Format(_T("Failed to load Green Channel Image File - %s"), filename);
				AfxMessageBox(message);
				btn = (CButton *)GetDlgItem(IDC_LOAD);
				btn->EnableWindow(TRUE);
				m_Status = _T("Failed to load green channel image");
				UpdateData(FALSE);
				return;
			}
		}
		else
		{
			btn = (CButton *)GetDlgItem(IDC_LOAD);
			btn->EnableWindow(TRUE);
			m_Status = _T("Failed to load green channel image");
			UpdateData(FALSE);
			return;
		}

		m_Status = _T("Loading Blue Channel Image, Please wait...");
		UpdateData(FALSE);

		CFileDialog dlg2(TRUE,    // open
			NULL,    // no default extension
			NULL,    // no initial file name
			OFN_FILEMUSTEXIST
			| OFN_HIDEREADONLY,
			_T("Blue Channel TIFF files (*.tif)|*.tif"), NULL, 0, TRUE);
		if (dlg2.DoModal() == IDOK)
		{
			CString filename = dlg2.GetPathName();
			UpdateData(FALSE);
			bool ret = m_BlueTIFFData->LoadRawTIFFFile(filename);
			if (!ret)
			{
				message.Format(_T("Failed to load Blue Channel Image File - %s"), filename);
				AfxMessageBox(message);
				btn = (CButton *)GetDlgItem(IDC_LOAD);
				btn->EnableWindow(TRUE);
				m_Status = _T("Failed to load blue channel image");
				UpdateData(FALSE);
				return;
			}
			else
				loaded = true;
		}
		else
		{
			btn = (CButton *)GetDlgItem(IDC_LOAD);
			btn->EnableWindow(TRUE);
			m_Status = _T("Failed to load blue channel image");
			UpdateData(FALSE);
			return;
		}

		if (loaded)
		{
			int width = m_RedTIFFData->GetSubsampledWidth();
			int height = m_RedTIFFData->GetSubsampledHeight();
			m_Image.Create(width, -height, 24);
			CopyToRGBImage();
			UpdateImageDisplay();

			HANDLE thread = ::CreateThread(NULL, 0, WBCScreenOperation, this, CREATE_SUSPENDED, NULL);

			if (!thread)
			{
				AfxMessageBox(_T("Fail to create thread for screening"));
				message.Format(_T("Failed to creat a thread for screening"));
				m_Log.Message(message);
				btn = (CButton *)GetDlgItem(IDC_LOAD);
				btn->EnableWindow(TRUE);
				return;
			}

			::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
			::ResumeThread(thread);
			message.Format(_T("Launched a thread for screening"));
			m_Log.Message(message);
			m_Status.Format(_T("Started Image Screening for WBCs"));
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}

		btn = (CButton *)GetDlgItem(IDC_LOAD);
		btn->EnableWindow(TRUE);
	}
}

void CWBCFinderDlg::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	(*rgnList).clear();
}

void CWBCFinderDlg::CopyToRGBImage()
{
	if (m_Image == NULL)
		return;
	if ((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && ((m_Image.GetHeight() == NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT)))
	{
		if ((m_HitIndex < 1) && (m_HitIndex > m_TotalHits))
			return;
		BYTE *pCursor = (BYTE*)m_Image.GetBits();
		int nPitch = m_Image.GetPitch();
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		for (int ImageLocationIndex = 0; ImageLocationIndex < NUM_IMAGES; ImageLocationIndex++, startIndex++)
		{
			if (startIndex < m_TotalHits)
			{
				CRGNData *rgnPtr = m_RGNDataList[startIndex];
				int regionWidth = rgnPtr->GetWidth();
				int regionHeight = rgnPtr->GetHeight();
				
				unsigned short *pBlueBuffer = rgnPtr->GetImage(BLUE_COLOR);
				unsigned short *pGreenBuffer = rgnPtr->GetImage(GREEN_COLOR);
				unsigned short *pRedBuffer = rgnPtr->GetImage(RED_COLOR);
				
				int blueMax = rgnPtr->GetContrast(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR);
				if (rgnPtr->m_BlueFrameMax > 0)
					blueMax = rgnPtr->m_BlueFrameMax;
				int redMax = rgnPtr->GetContrast(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR);
				if (rgnPtr->m_RedFrameMax > 0)
					redMax = rgnPtr->m_RedFrameMax;
				int greenMax = rgnPtr->GetContrast(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR);
				if (rgnPtr->m_GreenFrameMax > 0)
					greenMax = rgnPtr->m_GreenFrameMax;

				BYTE bluePixelValue = 0;
				BYTE greenPixelValue = 0;
				BYTE redPixelValue = 0;

				for (int y = 0; y < regionHeight; y++)
				{
					for (int x = 0; x < regionWidth; x++)
					{
						if (m_Blue.GetCheck() == BST_CHECKED)
							bluePixelValue = GetContrastEnhancedByte(*pBlueBuffer++, blueMax, rgnPtr->GetCutoff(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR));
						else
							bluePixelValue = (BYTE)0;
						if (m_Green.GetCheck() == BST_CHECKED)
							greenPixelValue = GetContrastEnhancedByte(*pGreenBuffer++, greenMax, rgnPtr->GetCutoff(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR));
						else
							greenPixelValue = (BYTE)0;
						if (m_Red.GetCheck() == BST_CHECKED)
							redPixelValue = GetContrastEnhancedByte(*pRedBuffer++, redMax, rgnPtr->GetCutoff(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR));
						else
							redPixelValue = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x)] = bluePixelValue;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 1] = greenPixelValue;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 2] = redPixelValue;
					}
				}

				if ((m_ShowBoundary.GetCheck() == BST_CHECKED) && (rgnPtr->GetBlobData(GB_COLORS)->size() > 0))
				{
					CBlobData *blob = (*rgnPtr->GetBlobData(GB_COLORS))[0];
					for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
					{
						int x0 = (*blob->m_Boundary)[j] % blob->m_Width;
						int y0 = (*blob->m_Boundary)[j] / blob->m_Width;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
					}
				}
			}
			else
			{
				for (int y = 0; y < SAMPLEFRAMEHEIGHT; y++)
				{
					for (int x = 0; x < SAMPLEFRAMEWIDTH; x++)
					{
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x)] = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 1] = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 2] = (BYTE)0;
					}
				}
			}
		}
	}
	else if ((m_Image.GetWidth() == m_RedTIFFData->GetSubsampledWidth()) && (m_Image.GetHeight() == m_RedTIFFData->GetSubsampledHeight()))
	{
		int width = m_RedTIFFData->GetSubsampledWidth();
		int height = m_RedTIFFData->GetSubsampledHeight();
		unsigned short *redImg = new unsigned short[width * height];
		unsigned short *greenImg = new unsigned short[width * height];
		unsigned short *blueImg = new unsigned short[width * height];
		bool retRed = m_RedTIFFData->GetSubsampleImage(redImg);
		bool retGreen = m_GreenTIFFData->GetSubsampleImage(greenImg);
		bool retBlue = m_BlueTIFFData->GetSubsampleImage(blueImg);
		int redCutoff = m_RedTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF];
		int redContrast = m_RedTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST];
		int greenCutoff = m_GreenTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF];
		int greenContrast = m_GreenTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST];
		int blueCutoff = m_BlueTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF];
		int blueContrast = m_BlueTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST];
		if (retRed && retGreen && retBlue)
		{
			BYTE *ptr = (BYTE *)m_Image.GetBits();
			int stride = m_Image.GetPitch() - 3 * width;
			unsigned short *ptrRed = redImg;
			unsigned short *ptrGreen = greenImg;
			unsigned short *ptrBlue = blueImg;
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++, ptrRed++, ptrGreen++, ptrBlue++)
				{
					BYTE redByte = GetContrastEnhancedByte(*ptrRed, redContrast, redCutoff);
					BYTE greenByte = GetContrastEnhancedByte(*ptrGreen, greenContrast, greenCutoff);
					BYTE blueByte = GetContrastEnhancedByte(*ptrBlue, blueContrast, blueCutoff);
					*ptr++ = blueByte;
					*ptr++ = greenByte;
					*ptr++ = redByte;
				}
				if (stride > 0)
					ptr += stride;
			}
		}
		delete[] redImg;
		delete[] greenImg;
		delete[] blueImg;
	}
}

BYTE CWBCFinderDlg::GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff)
{
	BYTE result = 0;
	int value1 = value - cutoff;
	int maxValue = (int)contrast;
	maxValue -= cutoff;
	if (maxValue < 255)
		maxValue = 255;

	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);;
	}
	return result;
}


void CWBCFinderDlg::OnBnClickedNexthit()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_NEXTHIT);
	btn->EnableWindow(FALSE);
	if ((m_Image != NULL) &&
		((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && (m_Image.GetHeight() == (NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT))))
	{
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		int totalPages = (m_TotalHits / NUM_IMAGES) + 1;
		if ((startIndex + NUM_IMAGES) <= (totalPages * NUM_IMAGES))
		{
			m_HitIndex += NUM_IMAGES;
			UpdateData(FALSE);
			CopyToRGBImage();
			UpdateImageDisplay();
			m_Status.Format(_T("Displayed %s Image, Hit No.%d"), m_ImageFilename, m_HitIndex);
			UpdateData(FALSE);
		}
		else
		{
			m_Status.Format(_T("Hit No.%d is out of range %d..%u"), startIndex + NUM_IMAGES, 1, m_TotalHits);
			UpdateData(FALSE);
		}
	}
	btn = (CButton *)GetDlgItem(IDC_NEXTHIT);
	btn->EnableWindow(TRUE);
}


void CWBCFinderDlg::OnBnClickedPrevhit()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if ((m_Image != NULL) &&
		((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && (m_Image.GetHeight() == (NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT))))
	{
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		if (startIndex >= NUM_IMAGES)
		{
			m_HitIndex -= NUM_IMAGES;
			UpdateData(FALSE);
			CopyToRGBImage();
			UpdateImageDisplay();
			m_Status.Format(_T("Displayed %s Image, Hit No.%d"), m_ImageFilename, m_HitIndex);
			UpdateData(FALSE);
		}
		else
		{
			m_Status.Format(_T("Hit No.%d is out of range %d..%u"), m_HitIndex - NUM_IMAGES, 1, m_TotalHits);
			UpdateData(FALSE);
		}
	}
}


void CWBCFinderDlg::OnBnClickedGotohit()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	if ((m_HitIndex > 0) && (m_HitIndex <= m_TotalHits))
	{
		CopyToRGBImage();
		UpdateImageDisplay();
		m_Status.Format(_T("Displayed %s Image, Hit No.%d"), m_ImageFilename, m_HitIndex);
		UpdateData(FALSE);
	}
	else
	{
		if ((m_Image != NULL) &&
			(m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && (m_Image.GetHeight() == (NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT)))
		{
			m_Status.Format(_T("Hit No.%d is out of range %d..%u"), m_HitIndex, 1, m_TotalHits);
			UpdateData(FALSE);
		}
	}
}

BOOL CWBCFinderDlg::PreTranslateMessage(MSG* pMsg)
{
	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN))
	{
		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT CWBCFinderDlg::OnMyMessage(WPARAM wparam, LPARAM lparam)
{
	UpdateData(FALSE);
	return 0;
}

BOOL CWBCFinderDlg::LoadRegionData(CString filename, vector<CRGNData *> *wbcList)
{
	bool ret = false;
	const CString TAG1 = _T("0 1, 1 ");
	const CString TAG2 = _T(", 2 ");
	const CString TAG3 = _T(", ");

	vector<CRGNData *> *list = wbcList;
	FreeRGNList(list);

	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			int index = textline.Find(TAG1);
			if (index > -1)
			{
				textline = textline.Mid(TAG1.GetLength());
				index = textline.Find(TAG2);
				if (index > -1)
				{
					unsigned int color = _wtoi(textline.Mid(0, index));
					textline = textline.Mid(index + TAG2.GetLength());
					index = textline.Find(_T(" "));
					if (index > -1)
					{
						int x0 = _wtoi(textline.Mid(0, index));
						textline = textline.Mid(index + 1);
						index = textline.Find(TAG3);
						if (index > -1)
						{
							ret = true;
							int y0 = _wtoi(textline.Mid(0, index));
							CRGNData *data = new CRGNData(x0, y0, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT);
							data->SetColorCode(color);
							data->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
							data->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
							data->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
							data->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
							data->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
							data->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
							data->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
							data->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
							data->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
							list->push_back(data);
							continue;
						}
						else
						{
							ret = false;
							break;
						}
					}
					else
					{
						ret = false;
						break;
					}
				}
				else
				{
					ret = false;
					break;
				}
			}
			else
			{
				ret = false;
				break;
			}
		}
		theFile.Close();
	}
	return ret;
}

DWORD WINAPI WBCScreenOperation(LPVOID param)
{
	CWBCFinderDlg *ptr = (CWBCFinderDlg *)param;
	ptr->m_HitFinder.ScanImage(&ptr->m_CTCParams, ptr->m_RedTIFFData, ptr->m_GreenTIFFData, ptr->m_BlueTIFFData, &ptr->m_RGNDataList);
	ptr->m_AverageCD45 = ptr->GetAverageCD45(&ptr->m_RGNDataList);
	if (ptr->m_Image != NULL)
		ptr->m_Image.Destroy();
	ptr->m_Image.Create(NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH, -(NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT), 24);
	ptr->m_TotalHits = (int)ptr->m_RGNDataList.size();
	ptr->m_HitIndex = 1;
	::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	CString message = _T("Before CopyToRGBImage()");
	ptr->m_Log.Message(message);
	ptr->CopyToRGBImage();
	message = _T("After CopyToRGBImage()");
	ptr->m_Log.Message(message);
	ptr->UpdateImageDisplay();
	ptr->m_Status.Format(_T("Completed Image Screen and found %d WBCs"), ptr->m_TotalHits);
	::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	WCHAR *char1 = _T("\\");
	int fileStringIndex = ptr->m_FullImagePathname.ReverseFind(*char1);
	CString filename_prefix = ptr->m_FullImagePathname.Mid(0, fileStringIndex + 1);
	CString regionFileName;
	regionFileName.Format(_T("%sWBC%sRun.rgn"), ptr->m_ImageFilename, CELLMAPPLUS_VERSION);
	regionFileName = filename_prefix + regionFileName;
	ptr->SaveWBCRegionFile(regionFileName);
	int index = ptr->m_FullImagePathname.Find(_T(".tif"));
	if (index > -1)
	{
		CString pathname = ptr->m_FullImagePathname.Mid(0, index);
		regionFileName.Format(_T("%s.wbc"), pathname);
		ptr->SaveWBCGreenAvg(regionFileName, ptr->m_AverageCD45);
	}
	regionFileName.Format(_T("%s_WBC_software_%d.csv"), ptr->m_ImageFilename, ptr->m_AverageCD45);
	regionFileName = filename_prefix + regionFileName;
	ptr->SaveExportFile(regionFileName);
	return 0;
}

int CWBCFinderDlg::GetAverageCD45(vector<CRGNData *> *wbcList)
{
	int numCells = 0;
	int totalAverage = 0;
	for (int i = 0; i < (int)wbcList->size(); i++)
	{
		CRGNData *ptr = (*wbcList)[i];
		if ((ptr != NULL) &&
			((int)ptr->GetBlobData(GB_COLORS)->size() > 0))
		{
			CBlobData *blob = (*ptr->GetBlobData(GB_COLORS))[0];
			if ((int)blob->m_Pixels->size() > 0)
			{
				numCells++;
				totalAverage += ptr->m_GreenAverage;
			}
		}
	}
	if (numCells > 0)
		totalAverage /= numCells;
	return totalAverage;
}

void CWBCFinderDlg::SaveWBCRegionFile(CString filenameForSave)
{
	CStdioFile theFile;

	if (theFile.Open(filenameForSave, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		int count = 0;
		for (int i = 0; i < (int)m_RGNDataList.size(); i++)
		{
			count++;
			CString singleLine;
			int x0, y0;
			m_RGNDataList[i]->GetPosition(&x0, &y0);
			singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
				WBC, x0, y0, count);
			theFile.WriteString(singleLine);
		}
		theFile.Close();
	}
}

void CWBCFinderDlg::OnBnClickedSavergn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if (m_TotalHits > 0)
	{
		CString filenameForSave;
		CString filenameToDisplay;
		CFileDialog dlg(FALSE,    // save
			CString(".rgn"),
			NULL,
			OFN_OVERWRITEPROMPT,
			_T("RGN files (*.rgn)|*.rgn||"), NULL, 0, TRUE);
		if (dlg.DoModal() == IDOK)
		{
			filenameForSave = dlg.GetPathName();
			filenameToDisplay = dlg.GetFileName();
			SaveWBCRegionFile(filenameForSave);
			m_Status.Format(_T("Save %u regions to %s file"), m_RGNDataList.size(), filenameToDisplay);
			UpdateData(FALSE);
		}
	}
}

void CWBCFinderDlg::SaveWBCGreenAvg(CString filename, int CD45Average)
{
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		CString textline;
		textline.Format(_T("WBCGREENAVG=%d\n"), CD45Average);
		theFile.WriteString(textline);
		theFile.Close();
	}
}

void CWBCFinderDlg::OnBnClickedRedcolor()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}


void CWBCFinderDlg::OnBnClickedGreen()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}


void CWBCFinderDlg::OnBnClickedBlue()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}


void CWBCFinderDlg::OnBnClickedShow()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}

void CWBCFinderDlg::OnBnClickedBatchrun()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	
	HANDLE thread = ::CreateThread(NULL, 0, WBCBatchRun, this, CREATE_SUSPENDED, NULL);
	CString message;

	if (!thread)
	{
		AfxMessageBox(_T("Fail to create thread for batch run"));
		message.Format(_T("Failed to creat a thread for batch run"));
		m_Log.Message(message);
		EnableButtons(TRUE);
		return;
	}

	::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
	::ResumeThread(thread);
	message.Format(_T("Launched a thread for batch run"));
	m_Log.Message(message);
	m_Status.Format(_T("Launched a thread for batch run"));
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
}

DWORD WINAPI WBCBatchRun(LPVOID param)
{
	CStdioFile theFile;
	CWBCFinderDlg *ptr = (CWBCFinderDlg *)param;
	CString message;
	bool success = true;

	if (theFile.Open(_T("WBCFinderBatch.txt"), CFile::modeRead | CFile::typeText))
	{
		CString textline;
		CStdioFile batchResFile;
		if (batchResFile.Open(_T("WBCFinderBatchRunResult.csv"), CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("Sample,SoftwareWBCs#,SoftwareCD45Average,GreenCPI,Time(sec),ManualWBC#,ManualCD45Average,CD45AverageDifference,CaptureRate(%%)\n"));
			batchResFile.WriteString(textline);
			batchResFile.Close();
		}

		int lineIdx = 0;
		while (theFile.ReadString(textline))
		{
			lineIdx++;
			int index = textline.Find(_T(","));
			if (index == -1)
			{
				ptr->m_Status.Format(_T("WBCFinderBatch.txt Line#%d Error: Fail to read pathname"), lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("WBCFinderBatch.txt Line#%d Error: Fail to read pathname"), lineIdx);
				ptr->m_Log.Message(message);
				success = false;
				break;
			}
			CString pathname = textline.Mid(0, index);
			textline = textline.Mid(index + 1);
			index = textline.Find(_T(","));
			if (index == -1)
			{
				ptr->m_Status.Format(_T("WBCFinderBatch.txt Line#%d Error: Fail to read image filename"), lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("WBCFinderBatch.txt Line#%d Error: Fail to read image filename"), lineIdx);
				ptr->m_Log.Message(message);
				success = false;
				break;
			}
			CString imagefilename = textline.Mid(0, index);
			textline = textline.Mid(index + 1);
			index = textline.Find(_T(","));
			BOOL doManual = false;
			int manualWBCCount = 0;
			int manualCD45Average = 0;
			float captureRate = 0.0F;
			CString manualRgnFilename = _T("");
			if (index > -1)
			{
				doManual = true;
				CString sampleName1;
				WCHAR ch1 = _T('\\');
				index = pathname.ReverseFind(ch1);
				if (index > -1)
				{
					sampleName1 = pathname.Mid(index + 1);
					manualRgnFilename = sampleName1 + _T("_WBC.rgn");
					manualRgnFilename = pathname + _T("\\") + manualRgnFilename;
				}
				else
					doManual = false;
			}
			int WBCCount = 0;
			int WBCAverage = 0;
			int GreenCPI = 0;
			float timeElapsed = 0.0F;
			CString sampleName;
			WCHAR ch1 = _T('\\');
			index = pathname.ReverseFind(ch1);
			if (index > -1)
			{
				sampleName = pathname.Mid(index+1);
				if (lineIdx == 1)
				{
					ptr->m_Status.Format(_T("Started %d)%s"), lineIdx, sampleName);
					::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					message.Format(_T("Started %d)%s"), lineIdx, sampleName);
					ptr->m_Log.Message(message);
				}
			}
			if (index > -1)
			{
				bool ret = ptr->ReadFilesAndRun(pathname + _T("\\") + imagefilename, imagefilename, &WBCCount, &WBCAverage, &timeElapsed, sampleName, &GreenCPI);
				if (doManual)
					doManual = ptr->CompareWithManualResult(manualRgnFilename, &manualWBCCount, &manualCD45Average, &captureRate);
				if (ret)
				{
					message.Format(_T("%s,%d,%d,%d,%.2f"), sampleName, WBCCount, WBCAverage, GreenCPI, timeElapsed);
					CString message1;
					if (doManual)
					{
						message1.Format(_T("%d,%d,%d,%.2f"), manualWBCCount, manualCD45Average, (WBCAverage - manualCD45Average), captureRate);
						message = message + _T(",") + message1;
					}
					message += _T("\n");
					if (batchResFile.Open(_T("WBCFinderBatchRunResult.csv"), CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
					{
						batchResFile.SeekToEnd();
						batchResFile.WriteString(message);
						batchResFile.Close();
					}
					message.Format(_T("(%d)%s,%d,%d,%d,%.2f(sec)"), lineIdx, sampleName, WBCCount, WBCAverage, GreenCPI, timeElapsed);
					if (doManual)
					{
						message1.Format(_T("%d,%d,%d,%.2f"), manualWBCCount, manualCD45Average, (WBCAverage - manualCD45Average), captureRate);
						message = message + _T(",") + message1;
					}
					ptr->m_Log.Message(message);
					ptr->m_Status.Format(_T("Completed %d)%s,%d,%d,%d,%.2f(sec)"), lineIdx, sampleName, WBCCount, WBCAverage, GreenCPI, timeElapsed);
					if (doManual)
					{
						message1.Format(_T("%d,%d,%d,%.2f"), manualWBCCount, manualCD45Average, (WBCAverage - manualCD45Average), captureRate);
						ptr->m_Status = ptr->m_Status + _T(",") + message1;
					}
					::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				}
				else
				{
					success = false;
					message.Format(_T("%d) failed"), lineIdx);
					ptr->m_Log.Message(message);
					ptr->m_Status.Format(_T("%d) failed"), lineIdx);
					::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					break;
				}
			}
		}
		theFile.Close();
	}

	if (success)
		ptr->m_Status.Format(_T("Batch Run Completed successfully"));
	else
		ptr->m_Status.Format(_T("Batch Run Failed, please check log file for the error happened"));
	::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	ptr->EnableButtons(TRUE);
	return 0;
}

bool CWBCFinderDlg::CompareWithManualResult(CString wbcFilename, int *manualWBCCount, int *manualCD45Average, float *captureRate)
{
	bool ret = false;
#if 0
	CString message;
	vector<CRGNData *> manualWBCs;
	BOOL doManual = LoadRegionData(wbcFilename, &manualWBCs);
	if (doManual)
	{
		ret = true;
		for (int i = 0; i < (int)manualWBCs.size(); i++)
		{
			unsigned short *redImage = new unsigned short[SAMPLEFRAMEWIDTH * SAMPLEFRAMEHEIGHT];
			unsigned short *greenImage = new unsigned short[SAMPLEFRAMEWIDTH * SAMPLEFRAMEHEIGHT];
			unsigned short *blueImage = new unsigned short[SAMPLEFRAMEWIDTH * SAMPLEFRAMEHEIGHT];
			int x0, y0;
			CRGNData *region = manualWBCs[i];
			region->GetPosition(&x0, &y0);
			BOOL retRed = m_RedTIFFData->GetImageRegion(x0, y0, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT, redImage);
			BOOL retGreen = m_GreenTIFFData->GetImageRegion(x0, y0, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT, greenImage);
			BOOL retBlue = m_BlueTIFFData->GetImageRegion(x0, y0, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT, blueImage);
			if (retRed && retGreen && retBlue)
			{
				region->SetImages(redImage, greenImage, blueImage);
				region->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
				region->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
				region->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
				region->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
				region->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
				region->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
				region->SetCPI(RED_COLOR, m_RedTIFFData->GetCPI());
				region->SetCPI(GREEN_COLOR, m_GreenTIFFData->GetCPI());
				region->SetCPI(BLUE_COLOR, m_BlueTIFFData->GetCPI());
				if (!m_HitFinder.ProcessOneRegionForWBCs(&m_CTCParams, region, m_RedTIFFData->IsZeissData()))
				{
					delete region;
					manualWBCs[i] = NULL;
				}
			}
		}
		vector<CRGNData *> manualWBCs1;
		for (int i = 0; i < (int)manualWBCs.size(); i++)
		{
			if (manualWBCs[i] != NULL)
			{
				manualWBCs1.push_back(manualWBCs[i]);
				manualWBCs[i] = NULL;
			}
		}
		if ((int)manualWBCs1.size() > 0)
		{
			ret = true;
			manualWBCs.clear();
		}
		if (!ret)
		{
			m_Status = _T("Failed to get Image Data for Manual CD45 Average Calculation.");
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			message.Format(_T("Failed to get Image Data for Manual CD45 Average Calculation."));
			m_Log.Message(message);
			return false;
		}
		*manualWBCCount = (int)manualWBCs1.size();
		*manualCD45Average = GetAverageCD45(&manualWBCs1);
		vector<POINT> manualPos;
		for (int i = 0; i < (int)manualWBCs1.size(); i++)
		{
			POINT pos = GetBlobCenter(manualWBCs1[i]);
			manualPos.push_back(pos);
		}
		vector<POINT> softwarePos;
		for (int i = 0; i < (int)m_RGNDataList.size(); i++)
		{
			POINT pos = GetBlobCenter(m_RGNDataList[i]);
			softwarePos.push_back(pos);
		}
		int capturedCount = 0;
		for (int i = 0; i < (int)manualPos.size(); i++)
		{
			int x0 = (int)manualPos[i].x;
			int y0 = (int)manualPos[i].y;
			for (int j = 0; j < (int)softwarePos.size(); j++)
			{
				int x1 = (int)softwarePos[j].x;
				int y1 = (int)softwarePos[j].y;
				if ((abs(x0 - x1) < 10) && (abs(y0 - y1) < 10))
				{
					capturedCount++;
					break;
				}
			}
		}
		*captureRate = (float)(100.0 * ((double)capturedCount) / ((double)manualWBCs1.size()));
		FreeRGNList(&manualWBCs1);
		manualPos.clear();
		softwarePos.clear();
	}
	else
	{
		m_Status.Format(_T("Failed to load Manual WBC Region File %s."), wbcFilename);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		message.Format(_T("Failed to load Manual WBC Region File %s."), wbcFilename);
		m_Log.Message(message);
		return false;
	}
	FreeRGNList(&manualWBCs);
#endif
	return ret;
}

bool CWBCFinderDlg::ReadFilesAndRun(CString filename, CString imagefilename, int *WBCCount, int *WBCAverage, float *timeElapsed, CString sampleName, 
	int *GreenCPI)
{
	bool ret = false;
	clock_t time1 = clock();
	FreeRGNList(&m_RGNDataList);
	CString message;
	bool loaded = false;

	ret = m_RedTIFFData->LoadRawTIFFFile(filename);
	if (!ret)
	{
		message.Format(_T("Failed to load %s"), imagefilename);
		m_Log.Message(message);
		m_Status.Format(_T("Failed to load %s"), imagefilename);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	else
	{
		message.Format(_T("Red TIFF File %s loaded successfully, Red CPI=%d"), filename, m_RedTIFFData->GetCPI());
		m_Log.Message(message);
		int index = imagefilename.Find(Leica_Red_Prefix, 0);
		if (index == -1)
		{
			int postfixIndex = filename.Find(Zeiss_Red_Postfix);
			if (postfixIndex == -1)
			{
				m_Status.Format(_T("Red TIFF Filename %s doesn't have PREFIX %s or POSTFIX %s"), imagefilename, Leica_Red_Prefix, Zeiss_Red_Postfix);
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
			else
			{
				CString samplePathName = filename.Mid(0, postfixIndex);
				CString filename2 = samplePathName + Zeiss_Green_Postfix;
				BOOL ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
				if (ret)
				{
					filename2 = samplePathName + Zeiss_Blue_Postfix;
					BOOL ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
					if (ret)
					{
						message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
						m_CTCParams.LoadCTCParams(message);
						loaded = true;
					}
					else
					{
						m_Status = _T("Failed to load Blue TIFF File");
						::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					}
				}
				else
				{
					m_Status = _T("Failed to load Green TIFF File");
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				}
			}
		}
		else
		{
			CString postfix = imagefilename.Mid(Leica_Red_Prefix.GetLength());
			index = filename.Find(Leica_Red_Prefix, 0);
			CString pathname = filename.Mid(0, index);
			CString filename2;
			filename2.Format(_T("%s%s%s"), pathname, Leica_Green_Prefix, postfix);
			ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
			if (!ret)
			{
				m_Status.Format(_T("Failed to load %s%s"), Leica_Green_Prefix, postfix);
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
			else
			{
				message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
				m_Log.Message(message);
				filename2.Format(_T("%s%s%s"), pathname, Leica_Blue_Prefix, postfix);
				ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
				if (!ret)
				{
					m_Status.Format(_T("Failed to load %s%s"), Leica_Blue_Prefix, postfix);
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				}
				else
				{
					message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
					m_Log.Message(message);
					message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
					m_CTCParams.LoadCTCParams(message);
					loaded = true;
				}
			}
		}
	}

	if (loaded)
	{
		m_HitFinder.ScanImage(&m_CTCParams, m_RedTIFFData, m_GreenTIFFData, m_BlueTIFFData, &m_RGNDataList);
		m_TotalHits = (int)m_RGNDataList.size();
		m_HitIndex = 1;
		m_AverageCD45 = GetAverageCD45(&m_RGNDataList);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
#if 0
		WCHAR *char1 = _T("\\");
		int fileStringIndex = filename.ReverseFind(*char1);
		CString filename_prefix = filename.Mid(0, fileStringIndex + 1);
		CString regionFileName;
		regionFileName.Format(_T("%sWBC%sRun.rgn"), sampleName, CELLMAPPLUS_VERSION);
		regionFileName = filename_prefix + regionFileName;
		SaveWBCRegionFile(regionFileName);
		int index = filename.Find(_T(".tif"));
		if (index > -1)
		{
			CString imagefilename1 = filename.Mid(0, index);
			regionFileName = imagefilename1 + _T(".wbc");
			SaveWBCGreenAvg(regionFileName, m_AverageCD45);
		}
		regionFileName.Format(_T("%s_WBC_software_%d.csv"), sampleName, m_AverageCD45);
		regionFileName = filename_prefix + regionFileName;
		SaveExportFile(regionFileName);
#endif
		*WBCCount = (int)m_RGNDataList.size();
		*WBCAverage = m_AverageCD45;
		*GreenCPI = m_GreenTIFFData->GetCPI();
	}
	clock_t time2 = clock();
	clock_t timediff = time2 - time1;
	*timeElapsed = ((float)timediff) / CLOCKS_PER_SEC;
	return loaded;
}

void CWBCFinderDlg::SaveExportFile(CString filenameForSave)
{
	CStdioFile theFile;
	if (theFile.Open(filenameForSave, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		CString singleLine;
		singleLine.Format(_T("RegionIndex,WBCPixels,CD45Average\n"));
		theFile.WriteString(singleLine);
		int totalAverage = 0;
		int numCells = 0;
		vector<int> average;
		for (int i = 0; i < (int)m_RGNDataList.size(); i++)
		{
			CRGNData *ptr = m_RGNDataList[i];
			if ((ptr != NULL) &&
				((int)ptr->GetBlobData(GB_COLORS)->size() > 0))
			{
				CBlobData *blob = (*ptr->GetBlobData(GB_COLORS))[0];
				if ((int)blob->m_Pixels->size() > 0)
				{
					numCells++;
					int numPixels = (int)blob->m_Pixels->size();
					int CD45Average = ptr->m_GreenAverage;
					singleLine.Format(_T("%d,%d,%d\n"), i + 1, (int)numPixels, CD45Average);
					theFile.WriteString(singleLine);
					average.push_back(CD45Average);
					totalAverage += CD45Average;
				}
			}
		}
		if (numCells > 0)
		{
			int averageCD45 = totalAverage / numCells;
			singleLine.Format(_T("AverageCD45,,%d\n"), averageCD45);
			theFile.WriteString(singleLine);
			float deviation = 0.0F;
			for (int i = 0; i < (int)average.size(); i++)
			{
				int dx = average[i] - averageCD45;
				deviation += (float)(dx * dx);
			}
			deviation = sqrt(deviation / numCells);
			singleLine.Format(_T("StdevCD45,,%.2f\n"), deviation);
			theFile.WriteString(singleLine);
			QuickSort(&average, 0, (int)average.size() - 1);
			int bright3percent = (int)average.size() * 2 / 3;
			numCells = 0;
			totalAverage = 0;
			for (int i = bright3percent; i < (int)average.size(); i++)
			{
				numCells++;
				totalAverage += average[i];
			}
			if (numCells > 0)
				totalAverage /= numCells;
			singleLine.Format(_T("Brighest3%%Average,,%d\n"), totalAverage);
			theFile.WriteString(singleLine);
		}
		theFile.Close();
		average.clear();
	}
}

template<typename T>
void CWBCFinderDlg::QuickSort(vector<T> *list, int lo, int hi)
{
	if (lo < hi)
	{
		int position = Partition(list, lo, hi);
		QuickSort(list, lo, position - 1);
		QuickSort(list, position + 1, hi);
	}
}

template<typename T>
void CWBCFinderDlg::Swap(vector<T> *list, int index1, int index2)
{
	T ptr = (*list)[index1];
	(*list)[index1] = (*list)[index2];
	(*list)[index2] = ptr;
}


int CWBCFinderDlg::Partition(vector<int> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	int pos = (*list)[position];
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i] < pos)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CWBCFinderDlg::ChoosePivot(vector<int> *list, int lo, int hi)
{
	int pivot = lo;
	int pos0 = (*list)[lo];
	int pos1 = (*list)[(lo + hi) / 2];
	int pos2 = (*list)[hi];
	if ((pos0 <= pos1) && (pos1 <= pos2))
		pivot = (lo + hi) / 2;
	else if ((pos0 <= pos2) && (pos2 <= pos1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
};

LRESULT CWBCFinderDlg::OnEnableButtons(WPARAM wparam, LPARAM lparam)
{
	EnableButtons(TRUE);
	return 0;
}

void CWBCFinderDlg::EnableButtons(BOOL enable)
{
	CButton *btn = (CButton *)GetDlgItem(IDC_BATCHRUN);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_NEXTHIT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_PREVHIT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_GOTOHIT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_REDCOLOR);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_GREEN);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_BLUE);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_SHOW);
	btn->EnableWindow(enable);
}