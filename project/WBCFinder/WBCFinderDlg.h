
// WBCFinderDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "RGNData.h"
#include "Log.h"
#include <vector>
#include "HitFindingOperation.h"
#include "CTCParams.h"
#include "SingleChannelTIFFData.h"

using namespace std;


// CWBCFinderDlg dialog
class CWBCFinderDlg : public CDialogEx
{
// Construction
public:
	CWBCFinderDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_WBCFINDER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	BYTE GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff);
	void PaintImages();

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_Display;
	CString m_Status;
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedLoad();
	CString m_ImageFilename;
	int m_HitIndex;
	int m_TotalHits;
	afx_msg void OnBnClickedNexthit();
	afx_msg void OnBnClickedPrevhit();
	afx_msg void OnBnClickedGotohit();
	BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg LRESULT OnMyMessage(WPARAM wparam, LPARAM lparam);
	afx_msg LRESULT OnEnableButtons(WPARAM wparam, LPARAM lparam);
	CHitFindingOperation m_HitFinder;
	vector<CRGNData *> m_RGNDataList;
	void CopyToRGBImage();
	CCTCParams m_CTCParams;
	void UpdateImageDisplay();
	CLog m_Log;
	CImage m_Image;
	CSingleChannelTIFFData *m_RedTIFFData;
	CSingleChannelTIFFData *m_GreenTIFFData;
	CSingleChannelTIFFData *m_BlueTIFFData;
	int m_AverageCD45;
	int GetAverageCD45(vector<CRGNData *> *wbcList);
	afx_msg void OnBnClickedSavergn();
	CButton m_ShowBoundary;
	CButton m_Blue;
	CButton m_Green;
	CButton m_Red;
	afx_msg void OnBnClickedRedcolor();
	afx_msg void OnBnClickedGreen();
	afx_msg void OnBnClickedBlue();
	afx_msg void OnBnClickedShow();
	afx_msg void OnBnClickedBatchrun();
	bool ReadFilesAndRun(CString filename, CString imagefilename, int *WBCCount, int *WBCAverage, float *timeElapsed, CString sampleName, int *GreenCPI);
	bool CompareWithManualResult(CString wbcFilename, int *manualWBCCount, int *manualCD45Average, float *captureRate);
	CString m_FullImagePathname;
	void SaveWBCGreenAvg(CString filename, int CD45Average);
	void SaveWBCRegionFile(CString filenameForSave);
	template<typename T> void QuickSort(vector<T> *list, int lo, int hi);
	template<typename T> void Swap(vector<T> *list, int index1, int index2);
	int Partition(vector<int> *list, int lo, int hi);
	int ChoosePivot(vector<int> *list, int lo, int hi);
	void SaveExportFile(CString filenameForSave);
	BOOL LoadRegionData(CString pathname, vector<CRGNData *> *wbcList);
	void FreeRGNList(vector<CRGNData *> *rgnList);
	void EnableButtons(BOOL enable);
	bool m_ZeissData;
	CString Leica_Red_Prefix;
	CString Leica_Green_Prefix;
	CString Leica_Blue_Prefix;
	CString Zeiss_Red_Postfix;
	CString Zeiss_Green_Postfix;
	CString Zeiss_Blue_Postfix;
	void LoadDefaultSettings();
	CButton m_LoadOneImageATime;
};
