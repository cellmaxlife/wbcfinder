//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 產生的 Include 檔案。
// 由 WBCFinder.rc 使用
//
#define IDD_WBCFINDER_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDC_DISPLAY                     1000
#define IDC_STATUS                      1001
#define IDC_LOAD                        1002
#define IDC_EDIT2                       1003
#define IDC_IMAGEFILE                   1003
#define IDC_NEXTHIT                     1004
#define IDC_TOTALHITS                   1005
#define IDC_PREVHIT                     1006
#define IDC_GOTOHIT                     1007
#define IDC_EDIT3                       1008
#define IDC_HITIDX                      1008
#define IDC_SAVERGN                     1009
#define IDC_EDIT1                       1010
#define IDC_CD45                        1010
#define IDC_REDCOLOR                    1011
#define IDC_GREEN                       1012
#define IDC_BLUE                        1013
#define IDC_CHECK4                      1014
#define IDC_SHOW                        1014
#define IDC_BUTTON1                     1015
#define IDC_BATCHTEST                   1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
