
// WBCFinderDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "RGNData.h"
#include "Log.h"
#include <vector>
#include "HitFindingOperation.h"
#include "CTCParams.h"

using namespace std;

// CWBCFinderDlg dialog
class CClusterFinderDlg : public CDialogEx
{
// Construction
public:
	CClusterFinderDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_WBCFINDER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	void FreeRGNList(vector<CRGNData *> *rgnList);
	BYTE GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff);
	void PaintImages(int index);
	
	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_Display;
	CString m_Status;
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedLoad();
	CString m_ImageFilename;
	int m_HitIndex;
	int m_TotalHits;
	afx_msg void OnBnClickedNexthit();
	afx_msg void OnBnClickedPrevhit();
	afx_msg void OnBnClickedGotohit();
	BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg LRESULT OnMyMessage(WPARAM wparam, LPARAM lparam);
	CHitFindingOperation m_HitFinder;
	vector<CRGNData *> m_RGNDataList;
	void CopyToRGBImage();
	CCTCParams m_CTCParams;
	void UpdateImageDisplay();
	CLog m_Log;
	CImage m_Image;
	CButton m_ShowBoundary;
	CButton m_Blue;
	CButton m_Green;
	CButton m_Red;
	afx_msg void OnBnClickedRedcolor();
	afx_msg void OnBnClickedGreen();
	afx_msg void OnBnClickedBlue();
	afx_msg void OnBnClickedShow();
	afx_msg void OnBnClickedBatchtest();
	bool m_ZeissData;
	void EnableButtons(BOOL enable);
	bool LoadAndProcessHitFile(CString sampleName);
};
