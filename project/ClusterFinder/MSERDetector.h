#include "CTCParams.h"
#include "RGNData.h"
#include "RegionDataSet.h"
#include "ColorType.h"
#include "BlobData.h"

class CMSERDetector
{
	template<typename T> void QuickSort(vector<T> *list, int lo, int hi);
	template<typename T> int Partition(vector<T> *list, int lo, int hi);
	template<typename T> int ChoosePivot(vector<T> *list, int lo, int hi);
	template<typename T> void Swap(vector<T> *list, int index1, int index2);
	int Find(int position, int *regionMap, int *regionLocation, int maxPosition);
	void Union(int regionIndex1, int regionIndex2, vector<REGIONDATASET *> *regions[], int index, int topIndex, int *regionMap, int maxPosition);
	void Detect(vector<REGIONDATASET *> *regions[], int topIndex, int bottomIndex, vector<CBlobData *> *blobList, 
		unsigned short *image, int width, int height, int IntensityDelta, int minCount, int maxCount);
	int GetSampleIndex(vector<REGIONDATASET *> singleRegion, int intensity, int intensityDelta);
	bool HasBeenVisisted(unsigned short reference, vector<unsigned short> visited);
	bool IsArtifact(INTENSITY_PIXEL_SET *intensityPixelSet, int width, int height);

public:
	CMSERDetector();
	virtual ~CMSERDetector();
	void GetRedBlobs(unsigned short *image, int width, int height, int minCount, int maxCount, int saturateCount, int IntensityDelta, vector<CBlobData *> *blobList);
};
