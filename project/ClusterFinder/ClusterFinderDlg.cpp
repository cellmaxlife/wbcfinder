﻿
// WBCFinderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ClusterFinder.h"
#include "ClusterFinderDlg.h"
#include "afxdialogex.h"
#include "VersionNumber.h"
#include "SingleRegionImageData.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CWBCFinderDlg dialog

#define NUM_IMAGE_COLUMNS	4
#define NUM_IMAGE_ROWS		4
#define NUM_IMAGES			16
#define WM_MY_MESSAGE (WM_USER+1001)
#define MAXINT_12BIT 4096
#define MAXINT_14BIT 16384
#define SAMPLEFRAMEWIDTH 100
#define SAMPLEFRAMEHEIGHT 100

static DWORD WINAPI HitBatchOperation(LPVOID param);

CClusterFinderDlg::CClusterFinderDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CClusterFinderDlg::IDD, pParent)
	, m_Status(_T(""))
	, m_ImageFilename(_T(""))
	, m_HitIndex(0)
	, m_TotalHits(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_Image.Create(NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH, -(NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT), 24);
}

void CClusterFinderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DISPLAY, m_Display);
	DDX_Text(pDX, IDC_STATUS, m_Status);
	DDX_Text(pDX, IDC_IMAGEFILE, m_ImageFilename);
	DDX_Text(pDX, IDC_HITIDX, m_HitIndex);
	DDX_Text(pDX, IDC_TOTALHITS, m_TotalHits);
	DDX_Control(pDX, IDC_SHOW, m_ShowBoundary);
	DDX_Control(pDX, IDC_BLUE, m_Blue);
	DDX_Control(pDX, IDC_GREEN, m_Green);
	DDX_Control(pDX, IDC_REDCOLOR, m_Red);
}

BEGIN_MESSAGE_MAP(CClusterFinderDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CClusterFinderDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_LOAD, &CClusterFinderDlg::OnBnClickedLoad)
	ON_BN_CLICKED(IDC_NEXTHIT, &CClusterFinderDlg::OnBnClickedNexthit)
	ON_BN_CLICKED(IDC_PREVHIT, &CClusterFinderDlg::OnBnClickedPrevhit)
	ON_BN_CLICKED(IDC_GOTOHIT, &CClusterFinderDlg::OnBnClickedGotohit)
	ON_MESSAGE(WM_MY_MESSAGE, OnMyMessage)
	ON_BN_CLICKED(IDC_REDCOLOR, &CClusterFinderDlg::OnBnClickedRedcolor)
	ON_BN_CLICKED(IDC_GREEN, &CClusterFinderDlg::OnBnClickedGreen)
	ON_BN_CLICKED(IDC_BLUE, &CClusterFinderDlg::OnBnClickedBlue)
	ON_BN_CLICKED(IDC_SHOW, &CClusterFinderDlg::OnBnClickedShow)
	ON_BN_CLICKED(IDC_BATCHTEST, &CClusterFinderDlg::OnBnClickedBatchtest)
END_MESSAGE_MAP()


// CWBCFinderDlg message handlers

BOOL CClusterFinderDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CString version;
	version.Format(_T("CTCClusterFinder(%s)"), CELLMAPPLUS_VERSION);
	m_Log.NewLog(version);
	m_HitFinder.m_Log = &m_Log;
	SetWindowText(version);
	version.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
	m_CTCParams.LoadCTCParams(version);
	m_Status = _T("Please Load Hit File");
	UpdateData(FALSE);
	m_Red.SetCheck(BST_CHECKED);
	m_Green.SetCheck(BST_CHECKED);
	m_Blue.SetCheck(BST_CHECKED);
	m_ShowBoundary.SetCheck(BST_CHECKED);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CClusterFinderDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		PaintImages(m_HitIndex);
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CClusterFinderDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CClusterFinderDlg::PaintImages(int hitIndex)
{
	if (m_Image == NULL)
		return;
	else if ((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && ((m_Image.GetHeight() == NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT)))
	{
		CPaintDC dc(&m_Display);
		CRect rect;
		m_Display.GetClientRect(&rect);
		dc.SetStretchBltMode(HALFTONE);
		m_Image.StretchBlt(dc.m_hDC, rect);
		CDC* pDC = m_Display.GetDC();
		CPen CursorPen(PS_SOLID, 3, RGB(255, 255, 255));
		CPen *pOldPen = pDC->SelectObject(&CursorPen);
		int blockWidth = (rect.right - rect.left + 1) / NUM_IMAGE_COLUMNS;
		for (int i = 1; i < NUM_IMAGE_COLUMNS; i++)
		{
			pDC->MoveTo(rect.left + i * blockWidth, rect.top);
			pDC->LineTo(rect.left + i * blockWidth, rect.bottom);
		}
		int blockHeight = (rect.bottom - rect.top + 1) / NUM_IMAGE_ROWS;
		for (int i = 1; i < NUM_IMAGE_ROWS; i++)
		{
			pDC->MoveTo(rect.left, rect.top + i * blockHeight);
			pDC->LineTo(rect.right, rect.top + i * blockHeight);
		}
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextAlign(TA_LEFT);
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		int length10um = 15 * blockWidth / 100;
		for (int i = 0; i < NUM_IMAGES; i++, startIndex++)
		{
			if (startIndex < m_TotalHits)
			{
				int ii = i / NUM_IMAGE_COLUMNS;
				int jj = i % NUM_IMAGE_COLUMNS;
				CRect rect1;
				rect1.left = rect.left + jj * blockWidth + 10;
				rect1.top = rect.top + ii * blockHeight + 10;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				CString label;
				label.Format(_T("%d"), (startIndex + 1));
				if (m_RGNDataList[startIndex]->GetColorCode() == WBC)
					pDC->SetTextColor(RGB(0, 255, 0));
				else
					pDC->SetTextColor(RGB(255, 0, 0));
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
				rect1.left = rect.left + jj * blockWidth + 10;
				rect1.top = rect.top + ii * blockHeight + 190;
				pDC->MoveTo(rect1.left, rect1.top);
				pDC->LineTo(rect1.left + length10um, rect1.top);
				pDC->SetTextColor(RGB(255, 255, 255));
				rect1.left = rect.left + jj * blockWidth + 10;
				rect1.top = rect.top + ii * blockHeight + 170;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				label.Format(_T("10um"));
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
				pDC->SetTextColor(RGB(255, 0, 0));
				rect1.left = rect.left + jj * blockWidth + 190;
				rect1.top = rect.top + ii * blockHeight + 10;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				label.Format(_T("R:%.2lf"), ((double)m_RGNDataList[startIndex]->m_RedAverage) / ((double)m_RGNDataList[startIndex]->GetCPI(RED_COLOR)));
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
				pDC->SetTextColor(RGB(0, 255, 0));
				rect1.left = rect.left + jj * blockWidth + 190;
				rect1.top = rect.top + ii * blockHeight + 30;
				rect1.right = rect1.left + 200;
				rect1.bottom = rect1.top + 20;
				label.Format(_T("G:%.2lf"), ((double)m_RGNDataList[startIndex]->m_GreenAverage) / ((double)m_RGNDataList[startIndex]->GetCPI(GREEN_COLOR)));
				pDC->DrawTextW(label, &rect1, DT_SINGLELINE | DT_NOCLIP);
			}
		}
		pDC->SelectObject(pOldPen);
	}
}

void CClusterFinderDlg::UpdateImageDisplay()
{
	RECT rect;
	m_Display.GetWindowRect(&rect);
	ScreenToClient(&rect);
	InvalidateRect(&rect, false);
}

void CClusterFinderDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRGNList(&m_RGNDataList);
	CDialogEx::OnCancel();
}

void CClusterFinderDlg::OnBnClickedLoad()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(FALSE);
	m_ImageFilename = _T("");
	FreeRGNList(&m_RGNDataList);
	m_TotalHits = 0;
	m_HitIndex = 0;
	m_Status = _T("Loading and Processing Image, Please wait...");
	UpdateData(FALSE);

	CString message;

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("Hit files (*.hit)|*.hit"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_ImageFilename = dlg.GetFileName();
		UpdateData(FALSE);
		
		CFile theFile;
		if (theFile.Open(filename, CFile::modeRead))
		{
			int CapturedCount = 0;
			CArchive archive(&theFile, CArchive::load);
			int version = 0;
			archive >> version;
			if (version != 3)
			{
				m_Status.Format(_T("Hits Image Data Archive Version Number (=%d) != 3"), version);
				UpdateData(FALSE);
				m_Status.Format(_T("Hit Image Data Archive File %s has a version number (=%d) != 3"), version);
				m_Log.Message(m_Status);
			}
			else
			{
				int totalCount = 0;
				archive >> totalCount;
				if (totalCount > 0)
				{
					int isZeissData;
					archive >> isZeissData;
					if (isZeissData == 0)
						m_ZeissData = false;
					else
						m_ZeissData = true;
					unsigned short CPI_Value;
					archive >> CPI_Value;
					int m_RedIntensity = CPI_Value;
					archive >> CPI_Value;
					int m_GreenIntensity = CPI_Value;
					archive >> CPI_Value;
					int m_BlueIntensity = CPI_Value;
					CString message;
					message.Format(_T("Hit Filename=%s, #ConfirmedCTCs=%d"), m_ImageFilename, totalCount);
					m_Log.Message(message);
					for (int i = 0; i < totalCount; i++)
					{
						CSingleRegionImageData *data = new CSingleRegionImageData();
						data->Serialize(archive);
						CRGNData *ptr = new CRGNData(data->m_RegionX0Pos, data->m_RegionY0Pos, data->m_RegionWidth, data->m_RegionHeight);
						ptr->SetImages(data->m_RedRegionImage, data->m_GreenRegionImage, data->m_BlueRegionImage);
						ptr->m_HitIndex = data->m_HitIndex + 1;
						data->m_RedRegionImage = NULL;
						data->m_GreenRegionImage = NULL;
						data->m_BlueRegionImage = NULL;
						delete data;
						ptr->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
						ptr->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
						ptr->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
						ptr->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
						ptr->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
						ptr->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
						ptr->SetCPI(RED_COLOR, m_RedIntensity);
						ptr->SetCPI(GREEN_COLOR, m_GreenIntensity);
						ptr->SetCPI(BLUE_COLOR, m_BlueIntensity);
						if (version == 3)
						{
							unsigned int color = ptr->GetColorCode();
							vector<CRGNData *> cellList;
							m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList, true);
							if (cellList.size() > 0)
							{
								m_HitFinder.GetCenterCell(ptr, &cellList, -1, -1, NULL, i + 1);
								FreeRGNList(&cellList);
							}
							else
							{
								ptr->SetScore(0);
								m_HitFinder.FreeBlobList(ptr->GetBlobData(RED_COLOR));
								m_HitFinder.FreeBlobList(ptr->GetBlobData(GREEN_COLOR));
								m_HitFinder.FreeBlobList(ptr->GetBlobData(BLUE_COLOR));
							}
							ptr->SetColorCode(color);
						}
					}
				}
				else
				{
					m_Status.Format(_T("NumHits (=%d) = 0"), totalCount);
					UpdateData(FALSE);
				}
			}
			archive.Close();
			theFile.Close();
			m_TotalHits = (int)m_RGNDataList.size();
			UpdateData(FALSE);
			if (m_TotalHits > 0)
			{
				m_HitIndex = 1;
				CopyToRGBImage();
				UpdateImageDisplay();
				m_Status.Format(_T("HitFile %s, Total Hits=%d, Captured Hits=%d"), m_ImageFilename, m_TotalHits, CapturedCount);
				UpdateData(FALSE);
			}
			else
			{
				m_Status.Format(_T("Hit File Loaded without finding any Confirmed CTCs"), m_ImageFilename);
				UpdateData(FALSE);
			}
		}
	}
	btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(TRUE);
}

void CClusterFinderDlg::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	(*rgnList).clear();
}

void CClusterFinderDlg::CopyToRGBImage()
{
	if (m_Image == NULL)
		return;
	if ((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && ((m_Image.GetHeight() == NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT)))
	{
		if ((m_HitIndex < 1) && (m_HitIndex > m_TotalHits))
			return;
		BYTE *pCursor = (BYTE*)m_Image.GetBits();
		int nPitch = m_Image.GetPitch();
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		for (int ImageLocationIndex = 0; ImageLocationIndex < NUM_IMAGES; ImageLocationIndex++, startIndex++)
		{
			if (startIndex < m_TotalHits)
			{
				CRGNData *rgnPtr = m_RGNDataList[startIndex];
				unsigned short *pBlueBuffer = rgnPtr->GetImage(BLUE_COLOR);
				unsigned short *pGreenBuffer = rgnPtr->GetImage(GREEN_COLOR);
				unsigned short *pRedBuffer = rgnPtr->GetImage(RED_COLOR);
				int regionWidth = rgnPtr->GetWidth();
				int regionHeight = rgnPtr->GetHeight();

				int blueMax = rgnPtr->GetContrast(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR);
				if (rgnPtr->m_BlueFrameMax > 0)
					blueMax = rgnPtr->m_BlueFrameMax;
				int redMax = rgnPtr->GetContrast(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR);
				if (rgnPtr->m_RedFrameMax > 0)
					redMax = rgnPtr->m_RedFrameMax;
				int greenMax = rgnPtr->GetContrast(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR);
				if (rgnPtr->m_GreenFrameMax > 0)
					greenMax = rgnPtr->m_GreenFrameMax;

				BYTE bluePixelValue = 0;
				BYTE greenPixelValue = 0;
				BYTE redPixelValue = 0;

				for (int y = 0; y < regionHeight; y++)
				{
					for (int x = 0; x < regionWidth; x++)
					{
						if (m_Blue.GetCheck() == BST_CHECKED)
							bluePixelValue = GetContrastEnhancedByte(*pBlueBuffer++, blueMax, rgnPtr->GetCutoff(BLUE_COLOR) + rgnPtr->GetCPI(BLUE_COLOR));
						else
							bluePixelValue = (BYTE)0;
						if (m_Green.GetCheck() == BST_CHECKED)
							greenPixelValue = GetContrastEnhancedByte(*pGreenBuffer++, greenMax, rgnPtr->GetCutoff(GREEN_COLOR) + rgnPtr->GetCPI(GREEN_COLOR));
						else
							greenPixelValue = (BYTE)0;
						if (m_Red.GetCheck() == BST_CHECKED)
							redPixelValue = GetContrastEnhancedByte(*pRedBuffer++, redMax, rgnPtr->GetCutoff(RED_COLOR) + rgnPtr->GetCPI(RED_COLOR));
						else
							redPixelValue = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x)] = bluePixelValue;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 1] = greenPixelValue;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 2] = redPixelValue;
					}
				}

				if (m_ShowBoundary.GetCheck() == BST_CHECKED)
				{
					if ((m_Red.GetCheck() == BST_CHECKED) && (m_Blue.GetCheck() == BST_CHECKED) && (rgnPtr->GetBlobData(RB_COLORS)->size() > 0))
					{
						CBlobData *blob = (*rgnPtr->GetBlobData(RB_COLORS))[0];
						for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
						{
							int x0 = (*blob->m_Boundary)[j] % blob->m_Width;
							int y0 = (*blob->m_Boundary)[j] / blob->m_Width;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
						}
					}
					else if ((m_Red.GetCheck() == BST_UNCHECKED) && (m_Blue.GetCheck() == BST_CHECKED) && (m_Green.GetCheck() == BST_CHECKED) && (rgnPtr->GetBlobData(GB_COLORS)->size() > 0))
					{
						CBlobData *blob = (*rgnPtr->GetBlobData(GB_COLORS))[0];
						for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
						{
							int x0 = (*blob->m_Boundary)[j] % blob->m_Width;
							int y0 = (*blob->m_Boundary)[j] / blob->m_Width;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
							pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
						}
					}
					else if ((m_Red.GetCheck() == BST_CHECKED) && (m_Blue.GetCheck() == BST_UNCHECKED) && (m_Green.GetCheck() == BST_UNCHECKED) && (rgnPtr->GetBlobData(RED_COLOR)->size() > 0))
					{
						for (int i = 0; i < (int)rgnPtr->GetBlobData(RED_COLOR)->size(); i++)
						{
							CBlobData *blob = (*rgnPtr->GetBlobData(RED_COLOR))[i];
							for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
							{
								int x0 = (*blob->m_Boundary)[j] % blob->m_Width;
								int y0 = (*blob->m_Boundary)[j] / blob->m_Width;
								pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
								pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
								pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
							}
						}
					}
					else if ((m_Red.GetCheck() == BST_UNCHECKED) && (m_Blue.GetCheck() == BST_CHECKED) && (m_Green.GetCheck() == BST_UNCHECKED) && (rgnPtr->GetBlobData(BLUE_COLOR)->size() > 0))
					{
						for (int i = 0; i < (int)rgnPtr->GetBlobData(BLUE_COLOR)->size(); i++)
						{
							CBlobData *blob = (*rgnPtr->GetBlobData(BLUE_COLOR))[i];
							for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
							{
								int x0 = (*blob->m_Boundary)[j] % blob->m_Width;
								int y0 = (*blob->m_Boundary)[j] / blob->m_Width;
								pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
								pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
								pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
							}
						}
					}
					else if ((m_Red.GetCheck() == BST_UNCHECKED) && (m_Blue.GetCheck() == BST_UNCHECKED) && (m_Green.GetCheck() == BST_CHECKED) && (rgnPtr->GetBlobData(GREEN_COLOR)->size() > 0))
					{
						for (int i = 0; i < (int)rgnPtr->GetBlobData(GREEN_COLOR)->size(); i++)
						{
							CBlobData *blob = (*rgnPtr->GetBlobData(GREEN_COLOR))[i];
							for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
							{
								int x0 = (*blob->m_Boundary)[j] % blob->m_Width;
								int y0 = (*blob->m_Boundary)[j] / blob->m_Width;
								pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0)] = (BYTE)255;
								pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 1] = (BYTE)255;
								pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y0) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x0) + 2] = (BYTE)255;
							}
						}
					}
				}
			}
			else
			{
				for (int y = 0; y < SAMPLEFRAMEHEIGHT; y++)
				{
					for (int x = 0; x < SAMPLEFRAMEWIDTH; x++)
					{
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x)] = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 1] = (BYTE)0;
						pCursor[nPitch * (SAMPLEFRAMEHEIGHT * (ImageLocationIndex / NUM_IMAGE_COLUMNS) + y) + 3 * (SAMPLEFRAMEWIDTH * (ImageLocationIndex % NUM_IMAGE_COLUMNS) + x) + 2] = (BYTE)0;
					}
				}
			}
		}
	}
}

BYTE CClusterFinderDlg::GetContrastEnhancedByte(unsigned short value, unsigned short contrast, int cutoff)
{
	BYTE result = 0;
	int value1 = value - cutoff;
	int maxValue = (int)contrast;
	maxValue -= cutoff;
	if (maxValue < 255)
		maxValue = 255;

	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);;
	}
	return result;
}


void CClusterFinderDlg::OnBnClickedNexthit()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CButton *btn = (CButton *)GetDlgItem(IDC_NEXTHIT);
	btn->EnableWindow(FALSE);
	if ((m_Image != NULL) &&
		((m_Image.GetWidth() == (2 * SAMPLEFRAMEWIDTH)) && (m_Image.GetHeight() == (2 * SAMPLEFRAMEHEIGHT))))
	{
		if ((m_HitIndex + 1) <= m_TotalHits)
		{
			m_HitIndex++;
			UpdateData(FALSE);
			CopyToRGBImage();
			UpdateImageDisplay();
			m_Status.Format(_T("Displayed %s Image, Hit No.%d"), m_ImageFilename, m_HitIndex);
			UpdateData(FALSE);
		}
		else
		{
			m_Status.Format(_T("Hit No.%d is out of range %d..%u"), m_HitIndex + 1, 1, m_TotalHits);
			UpdateData(FALSE);
		}
	}
	else if ((m_Image != NULL) &&
		((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && (m_Image.GetHeight() == (NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT))))
	{
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		int totalPages = (m_TotalHits / NUM_IMAGES) + 1;
		if ((startIndex + NUM_IMAGES) <= (totalPages * NUM_IMAGES))
		{
			m_HitIndex += NUM_IMAGES;
			UpdateData(FALSE);
			CopyToRGBImage();
			UpdateImageDisplay();
			m_Status.Format(_T("Displayed %s Image, Hit No.%d"), m_ImageFilename, m_HitIndex);
			UpdateData(FALSE);
		}
		else
		{
			m_Status.Format(_T("Hit No.%d is out of range %d..%u"), startIndex + NUM_IMAGES, 1, m_TotalHits);
			UpdateData(FALSE);
		}
	}
	btn = (CButton *)GetDlgItem(IDC_NEXTHIT);
	btn->EnableWindow(TRUE);
}


void CClusterFinderDlg::OnBnClickedPrevhit()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if ((m_Image != NULL) &&
		((m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && (m_Image.GetHeight() == (NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT))))
	{
		int pageIndex = m_HitIndex / NUM_IMAGES;
		if ((m_HitIndex % NUM_IMAGES) == 0)
			pageIndex++;
		int startIndex = pageIndex * NUM_IMAGES;
		if (startIndex >= NUM_IMAGES)
		{
			m_HitIndex -= NUM_IMAGES;
			UpdateData(FALSE);
			CopyToRGBImage();
			UpdateImageDisplay();
			m_Status.Format(_T("Displayed %s Image, Hit No.%d"), m_ImageFilename, m_HitIndex);
			UpdateData(FALSE);
		}
		else
		{
			m_Status.Format(_T("Hit No.%d is out of range %d..%u"), m_HitIndex - NUM_IMAGES, 1, m_TotalHits);
			UpdateData(FALSE);
		}
	}
}


void CClusterFinderDlg::OnBnClickedGotohit()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	if ((m_HitIndex > 0) && (m_HitIndex <= m_TotalHits))
	{
		CopyToRGBImage();
		UpdateImageDisplay();
		m_Status.Format(_T("Displayed %s Image, Hit No.%d"), m_ImageFilename, m_HitIndex);
		UpdateData(FALSE);
	}
	else
	{
		if ((m_Image != NULL) &&
			(m_Image.GetWidth() == (NUM_IMAGE_COLUMNS * SAMPLEFRAMEWIDTH)) && (m_Image.GetHeight() == (NUM_IMAGE_ROWS * SAMPLEFRAMEHEIGHT)))
		{
			m_Status.Format(_T("Hit No.%d is out of range %d..%u"), m_HitIndex, 1, m_TotalHits);
			UpdateData(FALSE);
		}
	}
}

BOOL CClusterFinderDlg::PreTranslateMessage(MSG* pMsg)
{
	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN))
	{
		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

LRESULT CClusterFinderDlg::OnMyMessage(WPARAM wparam, LPARAM lparam)
{
	UpdateData(FALSE);
	return 0;
}

void CClusterFinderDlg::OnBnClickedRedcolor()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}


void CClusterFinderDlg::OnBnClickedGreen()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}

void CClusterFinderDlg::OnBnClickedBlue()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}

void CClusterFinderDlg::OnBnClickedShow()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CopyToRGBImage();
	UpdateImageDisplay();
}

void CClusterFinderDlg::EnableButtons(BOOL enable)
{
	CButton *btn = (CButton *)GetDlgItem(IDC_BATCHTEST);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_LOAD);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_NEXTHIT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_PREVHIT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_GOTOHIT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_REDCOLOR);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_GREEN);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_BLUE);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_SHOW);
	btn->EnableWindow(enable);
}

void CClusterFinderDlg::OnBnClickedBatchtest()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);

	HANDLE thread = ::CreateThread(NULL, 0, HitBatchOperation, this, CREATE_SUSPENDED, NULL);
	CString message;

	if (!thread)
	{
		AfxMessageBox(_T("Fail to create thread for batch run"));
		message.Format(_T("Failed to creat a thread for batch run"));
		m_Log.Message(message);
		EnableButtons(TRUE);
		return;
	}

	::SetThreadPriority(thread, THREAD_PRIORITY_NORMAL);
	::ResumeThread(thread);
	message.Format(_T("Launched a thread for batch run"));
	m_Log.Message(message);
	m_Status.Format(_T("Launched a thread for batch run"));
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
}

DWORD WINAPI HitBatchOperation(LPVOID param)
{
	CStdioFile theFile;
	CClusterFinderDlg *ptr = (CClusterFinderDlg *)param;
	CString message;
	bool success = true;

	if (theFile.Open(_T("CellFinderBatch.txt"), CFile::modeRead | CFile::typeText))
	{
		CString textline;
		CStdioFile batchResFile;
		if (batchResFile.Open(_T("ConfirmedCTCAttributes.csv"), CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			textline.Format(_T("SampleName,RegionIndex,RedPixels,AverageRed,GreenPixels,AverageGreen,BluePixels,CellSize,NCRatio,GreenRing\n"));
			batchResFile.WriteString(textline);
			batchResFile.Close();
		}
		int lineIdx = 0;
		int samplesWithCTC = 0;
		while (theFile.ReadString(textline))
		{
			lineIdx++;
			int index = textline.Find(_T(","));
			if (index == -1)
			{
				ptr->m_Status.Format(_T("CellFinderBatch.txt Line#%d Error: Fail to read pathname"), lineIdx);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("CellFinderBatch.txt Line#%d Error: Fail to read pathname"), lineIdx);
				ptr->m_Log.Message(message);
				success = false;
				break;
			}
			CString pathname = textline.Mid(0, index);
			WCHAR *ch1 = _T("\\");
			index = pathname.ReverseFind(*ch1);
			CString sampleName = pathname.Mid(index + 1);
			ptr->m_Status.Format(_T("Started to process Hit File for Sample No.%d %s, %d"), lineIdx, sampleName, samplesWithCTC);
			::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			bool ret = ptr->LoadAndProcessHitFile(sampleName);
			if (ret)
			{
				int numCTCs = 0;
				if (batchResFile.Open(_T("ConfirmedCTCAttributes.csv"), CFile::modeNoTruncate | CFile::modeWrite | CFile::typeText))
				{
					batchResFile.SeekToEnd();
					//textline.Format(_T("SampleName,RegionIndex,RedPixels,AverageRed,GreenPixels,AverageGreen,BluePixels,CellSize,NCRatio,GreenRing\n"));
					for (int i = 0; i < (int)ptr->m_RGNDataList.size(); i++)
					{
						CRGNData *data = ptr->m_RGNDataList[i];
						if ((data->GetColorCode() == CTC) || (data->GetColorCode() == CTC2))
						{
							numCTCs++;
							message.Format(_T("%s,%d,%d,%d,%d,%d,%d,%.2f,%.2f,%.2f\n"), sampleName, data->m_HitIndex, data->GetPixels(RED_COLOR), data->m_RedValue, data->GetPixels(GREEN_COLOR), data->m_GreenValue, 
								data->GetPixels(BLUE_COLOR), data->m_CellSizeValue, data->m_NCRatio, data->m_GreenRingValue);
							batchResFile.WriteString(message);
						}
					}
					batchResFile.Close();
				}
				if (numCTCs > 0)
					samplesWithCTC++;
				ptr->m_Status.Format(_T("Sample No.%d %s: %d, numCTCs=%d"), lineIdx, sampleName, samplesWithCTC, numCTCs);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("Sample No.%d %s: samplesWithCTCs=%d, numCTCs=%d"), lineIdx, sampleName, samplesWithCTC, numCTCs);
				ptr->m_Log.Message(message);
				success = true;
			}
			else
			{
				ptr->m_Status.Format(_T("Failed to process Hit File for Sample No.%d %s"), lineIdx, sampleName);
				::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				message.Format(_T("Failed to process Hit File for Sample No.%d %s"), lineIdx, sampleName);
				ptr->m_Log.Message(message);
				success = false;
			}
		}
		theFile.Close();
	}
	else
	{
		ptr->m_Status.Format(_T("Failed to open CellFinderBatch.txt file"));
		::PostMessage(ptr->GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	ptr->EnableButtons(TRUE);
	return 0;
}

bool CClusterFinderDlg::LoadAndProcessHitFile(CString sampleName)
{
	bool ret = false;
	FreeRGNList(&m_RGNDataList);
	m_TotalHits = 0;
	CString message;
	CString filename;
	//filename.Format(_T("C:\\Users\\Xiaoming\\Documents\\HitFiles\\%sHIT.hit"), sampleName);
	filename.Format(_T("F:\\HitFiles\\%sHIT.hit"), sampleName);

	int totalCount = 0;
	CFile theFile;
	if (theFile.Open(filename, CFile::modeRead))
	{
		CArchive archive(&theFile, CArchive::load);
		int version = 0;
		archive >> version;
		if (version != 3)
		{
			m_Status.Format(_T("Hits Image Data Archive Version Number (=%d) != 3"), version);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			m_Status.Format(_T("Hit Image Data Archive File %s has a version number (=%d) != 3"), version);
			m_Log.Message(m_Status);
		}
		else
		{
			archive >> totalCount;
			if (totalCount > 0)
			{
				int isZeissData;
				archive >> isZeissData;
				if (isZeissData == 0)
					m_ZeissData = false;
				else
					m_ZeissData = true;
				unsigned short CPI_Value;
				archive >> CPI_Value;
				int m_RedIntensity = CPI_Value;
				archive >> CPI_Value;
				int m_GreenIntensity = CPI_Value;
				archive >> CPI_Value;
				int m_BlueIntensity = CPI_Value;
				for (int i = 0; i < totalCount; i++)
				{
					CSingleRegionImageData *data = new CSingleRegionImageData();
					data->Serialize(archive);
					CRGNData *ptr = new CRGNData(data->m_RegionX0Pos, data->m_RegionY0Pos, data->m_RegionWidth, data->m_RegionHeight);
					ptr->SetImages(data->m_RedRegionImage, data->m_GreenRegionImage, data->m_BlueRegionImage);
					ptr->m_HitIndex = data->m_HitIndex + 1;
					data->m_RedRegionImage = NULL;
					data->m_GreenRegionImage = NULL;
					data->m_BlueRegionImage = NULL;
					delete data;
					ptr->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
					ptr->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
					ptr->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
					ptr->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
					ptr->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
					ptr->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
					ptr->SetCPI(RED_COLOR, m_RedIntensity);
					ptr->SetCPI(GREEN_COLOR, m_GreenIntensity);
					ptr->SetCPI(BLUE_COLOR, m_BlueIntensity);
					if (version == 3)
					{
						unsigned int color = ptr->GetColorCode();
						vector<CRGNData *> cellList;
						m_HitFinder.ProcessOneROI(&m_CTCParams, ptr, &cellList, true);
						if (cellList.size() > 0)
						{
							m_HitFinder.GetCenterCell(ptr, &cellList, -1, -1, NULL, i + 1);
							FreeRGNList(&cellList);
						}
						else
						{
							ptr->SetScore(0);
							m_HitFinder.FreeBlobList(ptr->GetBlobData(RED_COLOR));
							m_HitFinder.FreeBlobList(ptr->GetBlobData(GREEN_COLOR));
							m_HitFinder.FreeBlobList(ptr->GetBlobData(BLUE_COLOR));
						}
						ptr->SetColorCode(color);
					}
				}
			}
			else
			{
				m_Status.Format(_T("NumHits (=%d) = 0"), totalCount);
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
		}
		archive.Close();
		theFile.Close();
		m_TotalHits = (int)m_RGNDataList.size();
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		if (m_TotalHits > 0)
		{
			m_Status.Format(_T("Sample %s, Total Hits=%d, Total CTCs=%d"), sampleName, totalCount, m_TotalHits);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			ret = true;
		}
		else
		{
			m_Status.Format(_T("Sample %s Hit File Loaded without finding any Cell Regions"), sampleName);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
	}

	return ret;
}