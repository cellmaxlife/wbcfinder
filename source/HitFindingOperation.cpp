#include "stdafx.h"
#include "HitFindingOperation.h"
#include <string.h>
#include "CTCParams.h"
#include "VersionNumber.h"
#include <math.h>
#include "SingleRegionImageData.h"

#define MIN_ENCOMPASS_PIXELS	20
#define MAXINT_12BIT 4096
#define MAXINT_14BIT 16384

CHitFindingOperation::CHitFindingOperation()
{
}

CHitFindingOperation::~CHitFindingOperation()
{
}

void CHitFindingOperation::FreeBlobList(vector<CBlobData *> *blobList)
{
	for (int i = 0; i < (int)blobList->size(); i++)
	{
		if ((*blobList)[i] != NULL)
			delete (*blobList)[i];
	}
	blobList->clear();
}

void CHitFindingOperation::GetBlobs(CRGNData *hitData)
{
	m_MSERDetector.GetBlobs(hitData->GetImage(RED_COLOR), hitData->GetBlobData(RED_COLOR));
	m_MSERDetector.GetBlobs(hitData->GetImage(GREEN_COLOR), hitData->GetBlobData(GREEN_COLOR));
	m_MSERDetector.GetBlobs(hitData->GetImage(BLUE_COLOR), hitData->GetBlobData(BLUE_COLOR));
}

void CHitFindingOperation::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	(*rgnList).clear();
}

void CHitFindingOperation::ErosionOperation(unsigned char *inImage, unsigned char *outImage, int width, int height)
{
	memset(outImage, 0, sizeof(unsigned char) * width * height);

	for (int i = 1; i < (height - 1); i++)
	{
		for (int j = 1; j < (width - 1); j++)
		{
			bool keep = true;
			for (int k = -1; k < 2; k++)
			{
				for (int h = -1; h < 2; h++)
				{
					if (inImage[width * (i + k) + (j + h)] != (unsigned char)255)
					{
						keep = false;
						break;
					}
				}
				if (!keep)
					break;
			}
			if (keep)
				outImage[width * i + j] = (unsigned char)255;
		}
	}
}

void CHitFindingOperation::GetBlobBoundary(CBlobData *blob)
{
	unsigned char *temp1 = new unsigned char[blob->m_Width * blob->m_Height];
	unsigned char *temp2 = new unsigned char[blob->m_Width * blob->m_Height];
	memset(temp2, 0, sizeof(unsigned char) * blob->m_Width * blob->m_Height);
	for (int i = 0; i < (int)blob->m_Pixels->size(); i++)
	{
		int x = (int)((*blob->m_Pixels)[i] % blob->m_Width);
		int y = (int)((*blob->m_Pixels)[i] / blob->m_Width);
		temp2[blob->m_Width * y + x] = (unsigned char)255;
	}
	ErosionOperation(temp2, temp1, blob->m_Width, blob->m_Height);
	blob->m_Boundary->clear();
	for (int i = 0; i < blob->m_Height; i++)
	{
		for (int j = 0; j < blob->m_Width; j++)
		{
			if ((temp2[blob->m_Width * i + j] == (unsigned char)255) &&
				(temp1[blob->m_Width * i + j] == (unsigned char)0))
			{
				int pos = (unsigned int) (i * blob->m_Width + j);
				blob->m_Boundary->push_back(pos);
			}
		}
	}
	delete[] temp1;
	delete[] temp2;
}

void CHitFindingOperation::ScanImage(CCTCParams *params, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage,
	CSingleChannelTIFFData *blueImage, vector<CRGNData *> *outputList)
{
	CleanUpExclusionArea(redImage, greenImage, blueImage);

	int regionWidth = SAMPLEFRAMEWIDTH;
	int regionHeight = SAMPLEFRAMEHEIGHT;

	int NumColumns = redImage->GetImageWidth() / regionWidth;
	int NumRows = redImage->GetImageHeight() / regionHeight;

	CString message;
	message.Format(_T("NumRows=%d,NumColumns=%d"), NumRows, NumColumns);
	m_Log->Message(message);

	unsigned short *redRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *greenRegionImage = new unsigned short[regionWidth * regionHeight];
	unsigned short *blueRegionImage = new unsigned short[regionWidth * regionHeight];

	vector<POINT> posList;
	for (int i = 0; i < NumRows; i++)
	{
		for (int j = 0; j < NumColumns; j++)
		{
			int x0 = regionWidth * j;
			int y0 = regionHeight * i;
			bool ret = blueImage->GetImageRegion(x0, y0, regionWidth, regionHeight, blueRegionImage);
			if (ret)
			{
				CRGNData *region = new CRGNData(x0, y0, regionWidth, regionHeight);
				region->SetImages(NULL, NULL, blueRegionImage);
				region->SetCPI(RED_COLOR, redImage->GetCPI());
				region->SetCPI(GREEN_COLOR, greenImage->GetCPI());
				region->SetCPI(BLUE_COLOR, blueImage->GetCPI());
				region->SetCutoff(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CUTOFF]);
				region->SetCutoff(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
				region->SetCutoff(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
				region->SetContrast(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CONTRAST]);
				region->SetContrast(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
				region->SetContrast(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
				region->m_RedFrameMax = region->GetCPI(RED_COLOR) + region->GetContrast(RED_COLOR);
				region->m_GreenFrameMax = region->GetCPI(GREEN_COLOR) + region->GetContrast(GREEN_COLOR);
				region->m_BlueFrameMax = region->GetCPI(BLUE_COLOR) + region->GetContrast(BLUE_COLOR);
				GetBlueBlobs(region, &posList);
				region->NullImages();
				delete region;
			}
		}
	}
	
	message.Format(_T("#BlueBlobs(Cell Region Cadidates)=%u"), posList.size());
	m_Log->Message(message);

	CFile archiveFile;
	if (archiveFile.Open(_T("test.hit"), CFile::modeCreate | CFile::modeWrite))
	{
		CArchive archive(&archiveFile, CArchive::store);
		archive << (int)3; // Version Number
		int count = (int)posList.size();
		archive << count;
		if (redImage->IsZeissData())
			archive << 1;
		else
			archive << 0;
		archive << redImage->GetCPI();
		archive << greenImage->GetCPI();
		archive << blueImage->GetCPI();
		for (int i = 0; i < (int)posList.size(); i++)
		{
			POINT pos = posList[i];
			int x0 = (int)pos.x - (SAMPLEFRAMEWIDTH / 2);
			int y0 = (int)pos.y - (SAMPLEFRAMEHEIGHT / 2);
			redImage->GetImageRegion(x0, y0, regionWidth, regionHeight, redRegionImage);
			greenImage->GetImageRegion(x0, y0, regionWidth, regionHeight, greenRegionImage);
			blueImage->GetImageRegion(x0, y0, regionWidth, regionHeight, blueRegionImage);
			CSingleRegionImageData *data = new CSingleRegionImageData();
			unsigned int color = WBC;
			data->m_HitIndex = i;
			data->m_ColorCode = color;
			data->m_RegionX0Pos = x0;
			data->m_RegionY0Pos = y0;
			data->m_RegionWidth = regionWidth;
			data->m_RegionHeight = regionHeight;
			data->m_RedRegionImage = redRegionImage;
			data->m_GreenRegionImage = greenRegionImage;
			data->m_BlueRegionImage = blueRegionImage;
			data->Serialize(archive);
			data->m_RedRegionImage = NULL;
			data->m_GreenRegionImage = NULL;
			data->m_BlueRegionImage = NULL;
			delete data;
		}
		archive.Close();
		archiveFile.Close();
	}
	
	message.Format(_T("Saved test.hit file"), posList.size());
	m_Log->Message(message);

	delete[] redRegionImage;
	delete[] greenRegionImage;
	delete[] blueRegionImage;
	posList.clear();

#if 0
	int histsize = MAXINT_12BIT;
	if (redImage->IsZeissData())
		histsize = MAXINT_14BIT;
	int *hist = new int[histsize];
	for (int i = 0; i < (int)posList.size(); i++)
	{
		POINT pos = posList[i];
		int x0 = (int)pos.x - (SAMPLEFRAMEWIDTH / 2);
		int y0 = (int)pos.y - (SAMPLEFRAMEHEIGHT / 2);
		bool retRed = redImage->GetImageRegion(x0, y0, regionWidth, regionHeight, redRegionImage);
		bool retGreen = greenImage->GetImageRegion(x0, y0, regionWidth, regionHeight, greenRegionImage);
		bool retBlue = blueImage->GetImageRegion(x0, y0, regionWidth, regionHeight, blueRegionImage);
		if (retRed && retGreen && retBlue)
		{
			CRGNData *region = new CRGNData(x0, y0, regionWidth, regionHeight);
			region->SetImages(redRegionImage, greenRegionImage, blueRegionImage);
			region->SetCPI(RED_COLOR, redImage->GetCPI());
			region->SetCPI(GREEN_COLOR, greenImage->GetCPI());
			region->SetCPI(BLUE_COLOR, blueImage->GetCPI());
			region->SetCutoff(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CUTOFF]);
			region->SetCutoff(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
			region->SetCutoff(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
			region->SetContrast(RED_COLOR, params->m_CTCParams[(int)PARAM_RED_CONTRAST]);
			region->SetContrast(GREEN_COLOR, params->m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
			region->SetContrast(BLUE_COLOR, params->m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
			region->m_RedFrameMax = region->GetCPI(RED_COLOR) + region->GetContrast(RED_COLOR);
			region->m_GreenFrameMax = region->GetCPI(GREEN_COLOR) + region->GetContrast(GREEN_COLOR);
			region->m_BlueFrameMax = region->GetCPI(BLUE_COLOR) + region->GetContrast(BLUE_COLOR);
			GetBlobs(region);
			FindMatchingBlobSize(region, pos);
			if ((region->m_RedBlueOverlaps > 0) || (region->m_GreenBlueOverlaps > 0))
			{
				MergeBlobs(region);
				if ((region->GetColorCode() == WBC) && (region->GetBlobData(GB_COLORS)->size() > 0))
				{ 
					CBlobData *blob = (*region->GetBlobData(GB_COLORS))[0];
					if ((blob != NULL) &&
						((int)blob->m_Pixels->size() >= params->m_CTCParams[(int)PARAM_MIN_BLOBPIXELCOUNT]) &&
						((int)blob->m_Pixels->size() <= params->m_CTCParams[(int)PARAM_MAX_BLOBPIXELCOUNT]) &&
						(IsCircularBlob(blob)) &&
						(IsBackgroundOK(region, blob, hist, histsize)))
					{
						outputList->push_back(region);
						region->SetPixels(GB_COLORS, (int)blob->m_Pixels->size());
						region->m_RedAverage = GetAverageIntensity(region->GetImage(RED_COLOR), blob->m_Pixels);
						region->m_RedValue = region->m_RedAverage - region->GetCPI(RED_COLOR);
						region->m_GreenAverage = GetAverageIntensity(region->GetImage(GREEN_COLOR), blob->m_Pixels);
						region->m_GreenValue = region->m_GreenAverage - region->GetCPI(GREEN_COLOR);
						region->m_RedFrameMax = 0;
						region->m_GreenFrameMax = region->m_GreenAverage;
						region->m_RedToGreenRatio = ((double)region->m_RedAverage) / ((double)region->m_GreenAverage);
						if (region->GetBlobData(BLUE_COLOR)->size() > 0)
						{
							region->SetPixels(BLUE_COLOR, (int)(*region->GetBlobData(BLUE_COLOR))[0]->m_Pixels->size());
							region->m_BlueAverage = GetAverageIntensity(region->GetImage(BLUE_COLOR), (*region->GetBlobData(BLUE_COLOR))[0]->m_Pixels);
							region->m_BlueValue = region->m_BlueAverage - region->GetCPI(BLUE_COLOR);
							region->m_BlueFrameMax = region->m_BlueAverage;
						}
						region->NullImages();
						region = NULL;
					}
				}
			}
			if (region != NULL)
			{
				region->NullImages();
				delete region;
			}
		}
	}

	delete[] hist;
	delete[] redRegionImage;
	delete[] greenRegionImage;
	delete[] blueRegionImage;
	posList.clear();

	RemoveDuplicates(outputList);

	QuickSort(outputList, 0, (int)outputList->size() - 1);

	for (int i = 0; i < (int)outputList->size() / 2; i++)
		Swap(outputList, i, (int)outputList->size() - 1 - i);

	for (int i = 0; i < (int)outputList->size(); i++)
	{
		CRGNData *ptr = (*outputList)[i];
		redRegionImage = new unsigned short[SAMPLEFRAMEWIDTH * SAMPLEFRAMEHEIGHT];
		greenRegionImage = new unsigned short[SAMPLEFRAMEWIDTH * SAMPLEFRAMEHEIGHT];
		blueRegionImage = new unsigned short[SAMPLEFRAMEWIDTH * SAMPLEFRAMEHEIGHT];
		ptr->SetImages(redRegionImage, greenRegionImage, blueRegionImage);
		int x0, y0;
		ptr->GetPosition(&x0, &y0);
		redImage->GetImageRegion(x0, y0, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT, redRegionImage);
		greenImage->GetImageRegion(x0, y0, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT, greenRegionImage);
		blueImage->GetImageRegion(x0, y0, SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT, blueRegionImage);
	}
#endif
}

int CHitFindingOperation::GetBlobPeakPosition(CBlobData *blob, unsigned short *image)
{
	int pos = 0;
	unsigned short maxIntensity = 0;
	
	for (int i = 0; i < (int)blob->m_Pixels->size(); i++)
	{
		if (image[(*blob->m_Pixels)[i]] > maxIntensity)
		{
			maxIntensity = image[(*blob->m_Pixels)[i]];
			pos = (*blob->m_Pixels)[i];
		}
	}

	return pos;
}

void CHitFindingOperation::GetBlueBlobs(CRGNData *region, vector<POINT> *posList)
{
	m_MSERDetector.GetBlobs(region->GetImage(BLUE_COLOR), region->GetBlobData(BLUE_COLOR));
	if (region->GetBlobData(BLUE_COLOR)->size() > 0)
	{
		int x0, y0;
		region->GetPosition(&x0, &y0);
		for (int i = 0; i < (int)region->GetBlobData(BLUE_COLOR)->size(); i++)
		{
			CBlobData *blob = (*region->GetBlobData(BLUE_COLOR))[i];
			if ((blob->m_Pixels->size() > 10) 
				&& (blob->m_Pixels->size() < 500)
				&& (blob->m_Threshold > (region->GetCPI(BLUE_COLOR) + 80)))
			{
				int pos = GetBlobPeakPosition(blob, region->GetImage(BLUE_COLOR));
				POINT pt;
				pt.x = x0 + (pos % blob->m_Width);
				pt.y = y0 + (pos / blob->m_Width);
				posList->push_back(pt);
			}
		}
		FreeBlobList(region->GetBlobData(BLUE_COLOR));
	}
}

bool CHitFindingOperation::IsBackgroundOK(CRGNData *ptr, CBlobData *blob, int *hist, int histsize)
{
	bool ret = false;
	while (true)
	{
		memset(hist, 0, sizeof(int) * histsize);
		unsigned short *imgPtr = ptr->GetImage(BLUE_COLOR);
		for (int k = 0; k < SAMPLEFRAMEWIDTH; k++)
			for (int h = 0; h < SAMPLEFRAMEHEIGHT; h++, imgPtr++)
				hist[(int)*imgPtr]++;
		int boundaryThreshold = SAMPLEFRAMEWIDTH * SAMPLEFRAMEHEIGHT / 10;
		if (hist[0] > boundaryThreshold)
			break;
		int blueThreshold = ptr->GetCPI(BLUE_COLOR) + ptr->GetCutoff(BLUE_COLOR) + 50;
		int pixelCountThreshold = 3000;
		CBlobData *blob1 = NULL;
		if (ptr->GetBlobData(BLUE_COLOR)->size() > 0)
			blob1 = (*ptr->GetBlobData(BLUE_COLOR))[0];
		if (blob1 != NULL)
		{
			blueThreshold = blob1->m_Threshold;
			pixelCountThreshold = 5 * (int)blob->m_Pixels->size();
		}
		int count = 0;
		for (int j = histsize - 1; j >= blueThreshold; j--)
			count += hist[j];
		if (count >= pixelCountThreshold)
			break;
		memset(hist, 0, sizeof(int) * histsize);
		if (ptr->GetBlobData(GREEN_COLOR)->size() > 0)
			blob1 = (*ptr->GetBlobData(GREEN_COLOR))[0];
		else
			blob1 = NULL;
		imgPtr = ptr->GetImage(GREEN_COLOR);
		for (int k = 0; k < SAMPLEFRAMEWIDTH; k++)
			for (int h = 0; h < SAMPLEFRAMEHEIGHT; h++, imgPtr++)
				hist[(int)*imgPtr]++;
		if (hist[0] > boundaryThreshold)
			break;
		int greenThreshold = ptr->GetCPI(GREEN_COLOR) + ptr->GetCutoff(GREEN_COLOR) + 50;
		pixelCountThreshold = 3000;
		if (blob1 != NULL)
		{
			greenThreshold = blob1->m_Threshold;
			pixelCountThreshold = 5 * (int)blob->m_Pixels->size();
		}
		count = 0;
		for (int j = histsize - 1; j >= greenThreshold; j--)
			count += hist[j];
		if (count >= pixelCountThreshold)
			break;
		if (ptr->GetBlobData(RED_COLOR)->size() > 0)
			blob1 = (*ptr->GetBlobData(RED_COLOR))[0];
		else
			blob1 = NULL;
		memset(hist, 0, sizeof(int) * histsize);
		imgPtr = ptr->GetImage(RED_COLOR);
		for (int k = 0; k < SAMPLEFRAMEWIDTH; k++)
			for (int h = 0; h < SAMPLEFRAMEHEIGHT; h++, imgPtr++)
				hist[(int)*imgPtr]++;
		if (hist[0] > boundaryThreshold)
			break;
		int redThreshold = ptr->GetCPI(RED_COLOR) + ptr->GetCutoff(RED_COLOR) + 50;
		pixelCountThreshold = 3000;
		if (blob1 != NULL)
		{
			redThreshold = blob1->m_Threshold;
			pixelCountThreshold = 5 * (int)blob->m_Pixels->size();
		}
		count = 0;
		for (int j = histsize - 1; j >= redThreshold; j--)
			count += hist[j];
		if (count >= pixelCountThreshold)
			break;
		ret = true;
		break;
	}
	return ret;
}

void CHitFindingOperation::MergeBlobs(CRGNData *hitData)
{
	CBlobData *redBlueBlob = NULL;
	CBlobData *greenBlueBlob = NULL;
	if (hitData->m_GreenBlueOverlaps > 0)
	{ 
		CBlobData *blob1 = (*hitData->GetBlobData(BLUE_COLOR))[0];
		CBlobData *blob2 = (*hitData->GetBlobData(GREEN_COLOR))[0];
		CBlobData *blob = new CBlobData(blob1->m_Width, blob1->m_Height);
		vector<int> pixels;
		for (int i = 0; i < (int)blob1->m_Pixels->size(); i++)
			pixels.push_back((*blob1->m_Pixels)[i]);
		for (int i = 0; i < (int)blob2->m_Pixels->size(); i++)
			pixels.push_back((*blob2->m_Pixels)[i]);
		QuickSort(&pixels, 0, (int)pixels.size() - 1);
		int index = 1;
		int lastPos = pixels[0];
		vector<int> *pixels1 = blob->m_Pixels;
		pixels1->push_back(lastPos);
		while (index < (int)pixels.size())
		{
			if (pixels[index] == lastPos)
				index++;
			else
			{
				lastPos = pixels[index];
				pixels1->push_back(lastPos);
				index++;
			}
		}
		pixels.clear();
		greenBlueBlob = blob;
	}
	if (hitData->m_RedBlueOverlaps > 0)
	{
		CBlobData *blob1 = (*hitData->GetBlobData(RED_COLOR))[0];
		CBlobData *blob2 = (*hitData->GetBlobData(BLUE_COLOR))[0];
		CBlobData *blob = new CBlobData(blob1->m_Width, blob1->m_Height);
		vector<int> pixels;
		for (int i = 0; i < (int)blob1->m_Pixels->size(); i++)
			pixels.push_back((*blob1->m_Pixels)[i]);
		for (int i = 0; i < (int)blob2->m_Pixels->size(); i++)
			pixels.push_back((*blob2->m_Pixels)[i]);
		QuickSort(&pixels, 0, (int)pixels.size() - 1);
		int index = 1;
		int lastPos = pixels[0];
		vector<int> *pixels1 = blob->m_Pixels;
		pixels1->push_back(lastPos);
		while (index < (int)pixels.size())
		{
			if (pixels[index] == lastPos)
				index++;
			else
			{
				lastPos = pixels[index];
				pixels1->push_back(lastPos);
				index++;
			}
		}
		pixels.clear();
		FreeBlobList(hitData->GetBlobData(RB_COLORS));
		redBlueBlob = blob;
	}

	if ((greenBlueBlob == NULL) && (redBlueBlob != NULL))
	{
		delete redBlueBlob;
	}
	else if ((redBlueBlob == NULL) && (greenBlueBlob != NULL))
	{
		hitData->GetBlobData(GB_COLORS)->push_back(greenBlueBlob);
		GetBlobBoundary(greenBlueBlob);
		hitData->SetColorCode(WBC);
	}
	else
	{
		int redAverage = GetAverageIntensity(hitData->GetImage(RED_COLOR), redBlueBlob->m_Pixels);
		if (redAverage < hitData->GetCPI(RED_COLOR))
		{
			delete redBlueBlob;
			hitData->GetBlobData(GB_COLORS)->push_back(greenBlueBlob);
			GetBlobBoundary(greenBlueBlob);
			hitData->SetColorCode(WBC);
			delete redBlueBlob;
		}
		else
		{
			delete redBlueBlob;
			delete greenBlueBlob;
		}
	}
}

int CHitFindingOperation::GetOverlapPixelCount(vector<int> *blob1, vector<int> *blob2)
{
	int count = 0;
	int index1 = 0;
	int index2 = 0;
	while ((index1 < (int)blob1->size()) && (index2 < (int)blob2->size()))
	{
		if ((*blob1)[index1] < (*blob2)[index2])
			index1++;
		else if ((*blob1)[index1] > (*blob2)[index2])
			index2++;
		else
		{
			count++;
			index1++;
			index2++;
		}
	}
	return count;
}

void CHitFindingOperation::FindMatchingBlobSize(CRGNData *hitData, POINT pos)
{
	if ((int)hitData->GetBlobData(BLUE_COLOR)->size() > 0)
	{
		int x0, y0;
		hitData->GetPosition(&x0, &y0);
		hitData->m_RedBlueOverlaps = 0;
		hitData->m_GreenBlueOverlaps = 0;
		int x1 = (int)pos.x - x0;
		int y1 = (int)pos.y - y0;
		int difference = MAXINT;
		int minIndex = -1;
		for (int k = 0; k < (int)hitData->GetBlobData(BLUE_COLOR)->size(); k++)
		{
			CBlobData *blueBlob = (*hitData->GetBlobData(BLUE_COLOR))[k];
			int loc = GetBlobPeakPosition(blueBlob, hitData->GetImage(BLUE_COLOR));
			int x2 = loc % blueBlob->m_Width;
			int y2 = loc / blueBlob->m_Width;
			if ((abs(x2 - x1) + abs(y2 - y1)) < difference)
			{
				difference = abs(x2 - x1) + abs(y2 - y1);
				minIndex = k;
			}
		}

		if (difference < 5)
		{
			CBlobData *blueBlob = (*hitData->GetBlobData(BLUE_COLOR))[minIndex];
			(*hitData->GetBlobData(BLUE_COLOR))[minIndex] = NULL;
			vector<CBlobData *> redBlobs;
			for (int h = 0; h < (int)hitData->GetBlobData(RED_COLOR)->size(); h++)
			{
				CBlobData *redBlob = (*hitData->GetBlobData(RED_COLOR))[h];
				(*hitData->GetBlobData(RED_COLOR))[h] = NULL;
				int overlappedPixels = GetOverlapPixelCount(blueBlob->m_Pixels, redBlob->m_Pixels);
				if (overlappedPixels > MIN_ENCOMPASS_PIXELS)
				{
					redBlobs.push_back(redBlob);
					hitData->m_RedBlueOverlaps += overlappedPixels;
				}
				else
					delete redBlob;
			}
			vector<CBlobData *> greenBlobs;
			for (int h = 0; h < (int)hitData->GetBlobData(GREEN_COLOR)->size(); h++)
			{
				CBlobData *greenBlob = (*hitData->GetBlobData(GREEN_COLOR))[h];
				(*hitData->GetBlobData(GREEN_COLOR))[h] = NULL;
				int overlappedPixels = GetOverlapPixelCount(blueBlob->m_Pixels, greenBlob->m_Pixels);
				if (overlappedPixels > MIN_ENCOMPASS_PIXELS)
				{
					greenBlobs.push_back(greenBlob);
					hitData->m_GreenBlueOverlaps += overlappedPixels;
				}
				else
					delete greenBlob;
			}

			FreeBlobList(hitData->GetBlobData(RED_COLOR));
			FreeBlobList(hitData->GetBlobData(GREEN_COLOR));
			FreeBlobList(hitData->GetBlobData(BLUE_COLOR));
			if ((hitData->m_RedBlueOverlaps > 0) || (hitData->m_GreenBlueOverlaps > 0))
			{
				hitData->GetBlobData(BLUE_COLOR)->push_back(blueBlob);
				for (int h = 0; h < (int)redBlobs.size(); h++)
				{
					hitData->GetBlobData(RED_COLOR)->push_back(redBlobs[h]);
					redBlobs[h] = NULL;
				}
				redBlobs.clear();
				for (int h = 0; h < (int)greenBlobs.size(); h++)
				{
					hitData->GetBlobData(GREEN_COLOR)->push_back(greenBlobs[h]);
					greenBlobs[h] = NULL;
				}
				greenBlobs.clear();
			}
			else
				delete blueBlob;
		}
		else
		{
			FreeBlobList(hitData->GetBlobData(RED_COLOR));
			FreeBlobList(hitData->GetBlobData(GREEN_COLOR));
			FreeBlobList(hitData->GetBlobData(BLUE_COLOR));
		}
	}
	else
	{
		FreeBlobList(hitData->GetBlobData(RED_COLOR));
		FreeBlobList(hitData->GetBlobData(GREEN_COLOR));
	}
}

template<typename T>
void CHitFindingOperation::QuickSort(vector<T> *list, int lo, int hi)
{
	if (lo < hi)
	{
		int position = Partition(list, lo, hi);
		QuickSort(list, lo, position - 1);
		QuickSort(list, position + 1, hi);
	}
}

int CHitFindingOperation::Partition(vector<int> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	int pos = (*list)[position];
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i] < pos)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CHitFindingOperation::ChoosePivot(vector<int> *list, int lo, int hi)
{
	int pivot = lo;
	int pos0 = (*list)[lo];
	int pos1 = (*list)[(lo + hi) / 2];
	int pos2 = (*list)[hi];
	if ((pos0 <= pos1) && (pos1 <= pos2))
		pivot = (lo + hi) / 2;
	else if ((pos0 <= pos2) && (pos2 <= pos1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
}

int CHitFindingOperation::Partition(vector<CRGNData *> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	double pos = (*list)[position]->m_RedToGreenRatio;
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i]->m_RedToGreenRatio < pos)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CHitFindingOperation::ChoosePivot(vector<CRGNData *> *list, int lo, int hi)
{
	int pivot = lo;
	double pos0 = (*list)[lo]->m_RedToGreenRatio;
	double pos1 = (*list)[(lo + hi) / 2]->m_RedToGreenRatio;
	double pos2 = (*list)[hi]->m_RedToGreenRatio;
	if ((pos0 <= pos1) && (pos1 <= pos2))
		pivot = (lo + hi) / 2;
	else if ((pos0 <= pos2) && (pos2 <= pos1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
}

template<typename T>
void CHitFindingOperation::Swap(vector<T> *list, int index1, int index2)
{
	T ptr = (*list)[index1];
	(*list)[index1] = (*list)[index2];
	(*list)[index2] = ptr;
}

void CHitFindingOperation::CleanUpExclusionArea(CSingleChannelTIFFData *redImg, CSingleChannelTIFFData *greenImg, CSingleChannelTIFFData *blueImg)
{
	const int ARRAYSIZE = 3000;

	int width = redImg->GetImageWidth();
	int height = redImg->GetImageHeight();

	RECT rect[12]; // 0: Left, 1: Right, 2:Top, 3:Bottom
	int width1;
	int height1;

	rect[0].left = 0;
	rect[0].right = ARRAYSIZE;
	rect[0].top = height / 2 - 25;
	rect[0].bottom = height / 2 + 25;

	rect[1].left = 0;
	rect[1].right = ARRAYSIZE;
	rect[1].top = height / 2 - 150;
	rect[1].bottom = height / 2 - 100;

	rect[2].left = 0;
	rect[2].right = ARRAYSIZE;
	rect[2].top = height / 2 + 100;
	rect[2].bottom = height / 2 + 150;

	rect[3].left = width - ARRAYSIZE;
	rect[3].right = width;
	rect[3].top = height / 2 - 25;
	rect[3].bottom = height / 2 + 25;

	rect[4].left = width - ARRAYSIZE;
	rect[4].right = width;
	rect[4].top = height / 2 - 150;
	rect[4].bottom = height / 2 - 100;

	rect[5].left = width - ARRAYSIZE;
	rect[5].right = width;
	rect[5].top = height / 2 + 100;
	rect[5].bottom = height / 2 + 150;

	rect[6].left = width / 2 - 25;
	rect[6].right = width / 2 + 25;
	rect[6].top = 0;
	rect[6].bottom = ARRAYSIZE;

	rect[7].left = width / 2 - 150;
	rect[7].right = width / 2 - 100;
	rect[7].top = 0;
	rect[7].bottom = ARRAYSIZE;

	rect[8].left = width / 2 + 100;
	rect[8].right = width / 2 + 150;
	rect[8].top = 0;
	rect[8].bottom = ARRAYSIZE;

	rect[9].left = width / 2 - 25;
	rect[9].right = width / 2 + 25;
	rect[9].top = height - ARRAYSIZE;
	rect[9].bottom = height;

	rect[10].left = width / 2 - 150;
	rect[10].right = width / 2 - 100;
	rect[10].top = height - ARRAYSIZE;
	rect[10].bottom = height;

	rect[11].left = width / 2 + 100;
	rect[11].right = width / 2 + 150;
	rect[11].top = height - ARRAYSIZE;
	rect[11].bottom = height;

	int left0 = 0;
	int right0 = 0;
	int top0 = 0;
	int bottom0 = 0;

	width1 = rect[0].right - rect[0].left;
	height1 = rect[0].bottom - rect[0].top;
	unsigned short *image = new unsigned short[width1 * height1];
	unsigned short *sumImage = new unsigned short[width1 * height1];
	int *profile[3];
	int size = sizeof(int) * ARRAYSIZE;
	for (int i = 0; i < 3; i++)
	{
		profile[i] = new int[ARRAYSIZE];
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		redImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		greenImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		blueImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i], 0, size);
		ImageProjection(sumImage, profile[i], width1, height1, true);
		LowpassFiltering(profile[i], ARRAYSIZE);
	}
	left0 = GetStepEdgePosition(profile, ARRAYSIZE, false) + 300;

	for (int i = 3; i < 6; i++)
	{
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		redImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		greenImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		blueImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i - 3], 0, size);
		ImageProjection(sumImage, profile[i - 3], width1, height1, true);
		LowpassFiltering(profile[i - 3], ARRAYSIZE);
	}

	right0 = rect[3].left + GetStepEdgePosition(profile, ARRAYSIZE, true) - 300;
	delete[] image;
	delete[] sumImage;

	width1 = rect[6].right - rect[6].left;
	height1 = rect[6].bottom - rect[6].top;
	image = new unsigned short[width1 * height1];
	sumImage = new unsigned short[width1 * height1];

	for (int i = 6; i < 9; i++)
	{
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		redImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		greenImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		blueImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i - 6], 0, size);
		ImageProjection(sumImage, profile[i - 6], width1, height1, false);
		LowpassFiltering(profile[i - 6], ARRAYSIZE);
	}

	top0 = GetStepEdgePosition(profile, ARRAYSIZE, false) + 300;

	for (int i = 9; i < 12; i++)
	{
		memset(sumImage, 0, sizeof(unsigned short) * width1 * height1);
		redImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		greenImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		blueImg->GetImageRegion(rect[i].left, rect[i].top, width1, height1, image);
		SumUpImageData(image, sumImage, width1, height1);
		memset(profile[i - 9], 0, size);
		ImageProjection(sumImage, profile[i - 9], width1, height1, false);
		LowpassFiltering(profile[i - 9], ARRAYSIZE);
	}

	bottom0 = rect[9].top + GetStepEdgePosition(profile, ARRAYSIZE, true) - 300;

	delete[] image;
	delete[] sumImage;
	for (int i = 0; i < 3; i++)
		delete[] profile[i];

	int centerX = (right0 + left0) / 2;
	int centerY = (top0 + bottom0) / 2;
	int radiusX = centerX - left0;
	int radiusY = centerY - top0;
	CString message;
	message.Format(_T("Exclusion: CenterX=%d, CenterY=%d, RadiusX=%d, RadiusY=%d"), centerX, centerY, radiusX, radiusY);
	m_Log->Message(message);
	message.Format(_T("Exclusion: Left0=%d, Top0=%d, Right0=%d, Bottom0=%d"), left0, top0, right0, bottom0);
	m_Log->Message(message);
	redImg->CleanUpExclusionArea(centerX, centerY, radiusX, radiusY);
	greenImg->CleanUpExclusionArea(centerX, centerY, radiusX, radiusY);
	blueImg->CleanUpExclusionArea(centerX, centerY, radiusX, radiusY);
}

void CHitFindingOperation::SumUpImageData(unsigned short *image, unsigned short *sumImage, int width, int height)
{
	unsigned short *ptrImage = image;
	unsigned short *ptrSum = sumImage;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, ptrImage++, ptrSum++)
		{
			*ptrSum += *ptrImage;
		}
	}
}

void CHitFindingOperation::ImageProjection(unsigned short *sumImage, int *profile, int width, int height, bool verticalProjection)
{
	if (verticalProjection)
	{
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				profile[i] += *(sumImage + width * j + i);
			}
		}
	}
	else
	{
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				profile[i] += *(sumImage + width * i + j);
			}
		}
	}
}

int CHitFindingOperation::GetStepEdgePosition(int *profile[], int length, bool startFromBeginning)
{
	int stepsize = 10;
	int length1 = length / stepsize;
	int *temp = new int[length1];
	memset(temp, 0, sizeof(int)* length1);
	if (startFromBeginning)
	{
		for (int i = 3; i < (length - 6); i++)
		{
			for (int j = 0; j < 3; j++)
			{
				temp[i / stepsize] += (*(profile[j] + i + 1) - *(profile[j] + i));
			}
		}
	}
	else
	{
		for (int i = (length - 6); i > 3; i--)
		{
			for (int j = 0; j < 3; j++)
			{
				temp[i / stepsize] += (*(profile[j] + i - 1) - *(profile[j] + i));
			}
		}
	}
	int max = 0;
	int maxi = 0;
	for (int i = 0; i < length1; i++)
	{
		if (temp[i] > max)
		{
			max = temp[i];
			maxi = i;
		}
	}
	delete[] temp;
	return (stepsize * maxi);
}

void CHitFindingOperation::LowpassFiltering(int *profile, int length)
{
	int *temp = new int[length - 8];
	temp[0] = 0;

	for (int i = 0; i < 7; i++)
		temp[0] += profile[i];

	for (int i = 1; i < (length - 8); i++)
		temp[i] = temp[i - 1] - profile[i] + profile[i + 7];

	for (int i = 0; i < 3; i++)
		profile[i] = 0;

	for (int i = 3; i < (length - 5); i++)
		profile[i] = temp[i - 3] / 7;

	for (int i = (length - 5); i < length; i++)
		profile[i] = 0;

	delete temp;
}

bool CHitFindingOperation::IsCircularBlob(CBlobData *blob)
{
	bool ret = false;
	
	int left = blob->m_Width;
	int right = 0;
	int top = blob->m_Height;
	int bottom = 0;
	for (int i = 0; i < (int)blob->m_Pixels->size(); i++)
	{
		int x = (*blob->m_Pixels)[i] % blob->m_Width;
		int y = (*blob->m_Pixels)[i] / blob->m_Width;
		if (x < left)
			left = x;
		if (x > right)
			right = x;
		if (y < top)
			top = y;
		if (y > bottom)
			bottom = y;
	}
	int width1 = (right - left + 1);
	int height1 = (bottom - top + 1);
	int longSide = width1;
	int shortSide = height1;
	if (height1 > longSide)
	{
		longSide = height1;
		shortSide = width1;
	}
	int aspectRatio = 100 * shortSide / longSide;
	int extent = 100 * (int)blob->m_Pixels->size() / longSide / shortSide;
	if ((aspectRatio > 35) && (extent > 65))
		ret = true;

	return ret;
}

int CHitFindingOperation::GetAverageIntensity(unsigned short *image, vector<int> *pixels)
{
	int intensity = 0;

	for (int i = 0; i < (int)pixels->size(); i++)
		intensity += image[(*pixels)[i]];

	if ((int)pixels->size() > 0)
		intensity /= (int)pixels->size();

	return intensity;
}

bool CHitFindingOperation::ProcessOneRegion(CCTCParams *params, CRGNData *region, bool isZeissData)
{
	bool ret = false;

	region->m_RedFrameMax = region->GetCPI(RED_COLOR) + region->GetContrast(RED_COLOR);
	region->m_GreenFrameMax = region->GetCPI(GREEN_COLOR) + region->GetContrast(GREEN_COLOR);
	region->m_BlueFrameMax = region->GetCPI(BLUE_COLOR) + region->GetContrast(BLUE_COLOR);
	GetBlobs(region);
	int x0, y0;
	region->GetPosition(&x0, &y0);
	POINT pos;
	pos.x = x0 + SAMPLEFRAMEWIDTH / 2;
	pos.y = y0 + SAMPLEFRAMEHEIGHT / 2;
	FindMatchingBlobSize(region, pos);
	if ((region->m_RedBlueOverlaps > 0) || (region->m_GreenBlueOverlaps > 0))
	{
		ret = true;
		MergeBlobs(region);
		CBlobData *blob = NULL;
		if (region->GetColorCode() == CTC)
		{
			blob = (*region->GetBlobData(RB_COLORS))[0];
			region->SetPixels(RB_COLORS, (int)blob->m_Pixels->size());
			region->m_RedAverage = GetAverageIntensity(region->GetImage(RED_COLOR), blob->m_Pixels);
			region->m_RedValue = region->m_RedAverage - region->GetCPI(RED_COLOR);
			region->m_GreenAverage = GetAverageIntensity(region->GetImage(GREEN_COLOR), blob->m_Pixels);
			region->m_GreenValue = region->m_GreenAverage - region->GetCPI(GREEN_COLOR);
			region->m_RedFrameMax = region->m_RedAverage;
			region->m_GreenFrameMax = 0;
		}
		else
		{
			blob = (*region->GetBlobData(GB_COLORS))[0];
			region->SetPixels(GB_COLORS, (int)blob->m_Pixels->size());
			region->m_RedAverage = GetAverageIntensity(region->GetImage(RED_COLOR), blob->m_Pixels);
			region->m_RedValue = region->m_RedAverage - region->GetCPI(RED_COLOR);
			region->m_GreenAverage = GetAverageIntensity(region->GetImage(GREEN_COLOR), blob->m_Pixels);
			region->m_GreenValue = region->m_GreenAverage - region->GetCPI(GREEN_COLOR);
			region->m_RedFrameMax = 0;
			region->m_GreenFrameMax = region->m_GreenAverage;
		}
		region->m_RedToGreenRatio = ((double)region->m_RedAverage) / ((double)region->m_GreenAverage);
		if (region->GetBlobData(BLUE_COLOR)->size() > 0)
		{
			region->SetPixels(BLUE_COLOR, (int)(*region->GetBlobData(BLUE_COLOR))[0]->m_Pixels->size());
			region->m_BlueAverage = GetAverageIntensity(region->GetImage(BLUE_COLOR), (*region->GetBlobData(BLUE_COLOR))[0]->m_Pixels);
			region->m_BlueValue = region->m_BlueAverage - region->GetCPI(BLUE_COLOR);
			region->m_BlueFrameMax = region->m_BlueAverage;
		}
	}
	return ret;
}

void CHitFindingOperation::RemoveDuplicates(vector<CRGNData *> *cellList)
{
	vector<CRGNData *> list;
	for (int i = 0; i < (int)cellList->size() - 1; i++)
	{
		int x0, y0;
		(*cellList)[i]->GetPosition(&x0, &y0);
		bool matched = false;
		for (int j = i + 1; j < (int)cellList->size(); j++)
		{
			int x1, y1;
			(*cellList)[j]->GetPosition(&x1, &y1);
			if ((abs(x0 - x1) < 10) && (abs(y0 - y1) < 10))
			{
				matched = true;
				break;
			}
		}
		if (!matched)
		{
			list.push_back((*cellList)[i]);
			(*cellList)[i] = NULL;
		}
	}
	list.push_back((*cellList)[(int)cellList->size() - 1]);
	(*cellList)[(int)cellList->size() - 1] = NULL;
	FreeRGNList(cellList);
	for (int i = 0; i < (int)list.size(); i++)
	{
		cellList->push_back(list[i]);
		list[i] = NULL;
	}
	FreeRGNList(&list);
}